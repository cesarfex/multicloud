from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver


AWS_EC2_ACCESS_ID = ''
AWS_EC2_SECRET_KEY = ''
AWS_REGION='us-east-1'

cls = get_driver(Provider.EC2)

driver = cls(AWS_EC2_ACCESS_ID, AWS_EC2_SECRET_KEY, region='us-east-1')

snapshotID = 'snap-014800e30d08d711a'
volumeID = 'vol-015371ddd413f1756'

volumes = driver.list_volumes()

for volume in volumes:
    if volume.id == volumeID:
        snapshots = driver.list_volume_snapshots(volume)
        for snapshot in snapshots:
            test = driver.destroy_volume_snapshot(snapshot)
            print(test)
