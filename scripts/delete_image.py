from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

AWS_EC2_ACCESS_ID = ''
AWS_EC2_SECRET_KEY = ''
AWS_REGION='us-east-1'
imageNAME = 'ami-032d4de2ca5d2e550'

cls = get_driver(Provider.EC2)

driver = cls(AWS_EC2_ACCESS_ID, AWS_EC2_SECRET_KEY, region='us-east-1')

image = driver.get_image(imageNAME)
print(image)

delete = driver.delete_image(image)
print(delete)