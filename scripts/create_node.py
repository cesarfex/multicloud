from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.compute.base import NodeImage


AWS_EC2_ACCESS_ID = ''
AWS_EC2_SECRET_KEY = ''
AWS_REGION='us-east-1'
AMI_ID = 'ami-013e2b5127d181b0c'
SIZE_ID = 'm1.small'
NAME = 'Teste123'
keyname = 'funchal'

# 'us-west-1' region is available in Libcloud under EC2_US_WEST provider
# constant

cls = get_driver(Provider.EC2)
driver = cls(AWS_EC2_ACCESS_ID,  AWS_EC2_SECRET_KEY , AWS_REGION)
sizes = driver.list_sizes()
size = [s for s in sizes if s.id == SIZE_ID][0]
image = NodeImage(id=AMI_ID, name=None, driver=driver)
node = driver.create_node(name=NAME, image=image, size=size, ex_keyname=keyname)

