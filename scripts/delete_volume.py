from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver


AWS_EC2_ACCESS_ID = ''
AWS_EC2_SECRET_KEY = ''
AWS_REGION='us-east-1'

cls = get_driver(Provider.EC2)

driver = cls(AWS_EC2_ACCESS_ID, AWS_EC2_SECRET_KEY, region='us-east-1')

location = driver.list_locations()[0]

print(location)

#volume = driver.create_volume(name='teste', size=10, location=location, ex_volume_type = 'gp2')
volumes = driver.list_volumes()

print(volumes)
id = 'vol-05e19a7fe91aa0e98'

for volume in volumes:
    print(volume.id)
    if volume.id == id:
        print("Achei...")
        delete = driver.destroy_volume(volume)
