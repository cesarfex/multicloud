from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

AWS_EC2_ACCESS_ID = ''
AWS_EC2_SECRET_KEY = ''
AWS_REGION='us-east-1'
instanceID = 'i-0581f78effbfa6773'
name = 'copia_'+instanceID
print(name)

cls = get_driver(Provider.EC2)

driver = cls(AWS_EC2_ACCESS_ID, AWS_EC2_SECRET_KEY, region='us-east-1')

instances = driver.list_nodes()
print(instances)

for instance in instances:
    print(instance)
    if instance.id == instanceID:
        image = driver.create_image(instance, name)
        print(image)
        print(image.id)

