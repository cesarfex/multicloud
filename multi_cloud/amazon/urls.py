from django.urls import path, include
from rest_framework import routers

from . import api
from . import views

router = routers.DefaultRouter()
router.register(r'credentials', api.CREDENTIALSViewSet)
router.register(r'instance', api.INSTANCEViewSet)
router.register(r'volumes', api.VOLUMESViewSet)
router.register(r'keys', api.KEYSViewSet)
router.register(r'images', api.IMAGESViewSet)
router.register(r'snapshots', api.SNAPSHOTSViewSet)

urlpatterns = (
    # urls for Django Rest Framework API
    path('api/v1/', include(router.urls)),
)

urlpatterns += (
    # urls for CREDENTIALS
    path('amazon/credentials/', views.CREDENTIALSListView.as_view(), name='amazon_credentials_list'),
    path('amazon/credentials/create/', views.CREDENTIALSCreateView.as_view(), name='amazon_credentials_create'),
    path('amazon/credentials/detail/<int:id>/', views.CREDENTIALSDetailView.as_view(), name='amazon_credentials_detail'),
    path('amazon/credentials/update/<int:id>/', views.CREDENTIALSUpdateView.as_view(), name='amazon_credentials_update'),
)

urlpatterns += (
    # urls for INSTANCE
    path('amazon/instance/', views.INSTANCEListView.as_view(), name='amazon_instance_list'),
    path('amazon/instance/create/', views.INSTANCECreateView.as_view(), name='amazon_instance_create'),
    path('amazon/instance/detail/<int:id>/', views.INSTANCEDetailView.as_view(), name='amazon_instance_detail'),
    path('amazon/instance/update/<int:id>/', views.INSTANCEUpdateView.as_view(), name='amazon_instance_update'),
)

urlpatterns += (
    # urls for VOLUMES
    path('amazon/volumes/', views.VOLUMESListView.as_view(), name='amazon_volumes_list'),
    path('amazon/volumes/create/', views.VOLUMESCreateView.as_view(), name='amazon_volumes_create'),
    path('amazon/volumes/detail/<int:id>/', views.VOLUMESDetailView.as_view(), name='amazon_volumes_detail'),
    path('amazon/volumes/update/<int:id>/', views.VOLUMESUpdateView.as_view(), name='amazon_volumes_update'),
)


urlpatterns += (
    # urls for Keys
    path('amazon/volumes/', views.KEYSListView.as_view(), name='amazon_keys_list'),
    path('amazon/volumes/create/', views.KEYSCreateView.as_view(), name='amazon_keys_create'),
    path('amazon/volumes/detail/<int:id>/', views.KEYSDetailView.as_view(), name='amazon_keys_detail'),
    path('amazon/volumes/update/<int:id>/', views.KEYSUpdateView.as_view(), name='amazon_keys_update'),
)

urlpatterns += (
    # urls for Images
    path('amazon/images/', views.IMAGESListView.as_view(), name='amazon_images_list'),
    path('amazon/images/create/', views.IMAGESCreateView.as_view(), name='amazon_images_create'),
    path('amazon/images/detail/<int:id>/', views.IMAGESDetailView.as_view(), name='amazon_images_detail'),
    path('amazon/images/update/<int:id>/', views.IMAGESUpdateView.as_view(), name='amazon_images_update'),
)

urlpatterns += (
    # urls for SNAPSHOT
    path('amazon/snapshots/', views.SNAPSHOTSListView.as_view(), name='amazon_snapshots_list'),
    path('amazon/snapshots/create/', views.SNAPSHOTSCreateView.as_view(), name='amazon_snapshots_create'),
    path('amazon/snapshots/detail/<int:id>/', views.SNAPSHOTSDetailView.as_view(), name='amazon_snapshots_detail'),
    path('amazon/snapshots/update/<int:id>/', views.SNAPSHOTSUpdateView.as_view(), name='amazon_snapshots_update'),
)