import unittest
from django.urls import reverse
from django.test import Client
from .models import CREDENTIALS, INSTANCE, OUTSCALE_INSTANCE_TYPES, REGIONS
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_credentials(**kwargs):
    defaults = {}
    defaults["AWS_EC2_ACCESS_ID"] = "AWS_EC2_ACCESS_ID"
    defaults["AWS_EC2_SECRET_KEY"] = "AWS_EC2_SECRET_KEY"
    defaults.update(**kwargs)
    return CREDENTIALS.objects.create(**defaults)


def create_instance(**kwargs):
    defaults = {}
    defaults["AWS_REGION"] = "AWS_REGION"
    defaults["AMI_ID"] = "AMI_ID"
    defaults["SIZE_ID"] = "SIZE_ID"
    defaults["NAME"] = "NAME"
    defaults["NODE_STATE_MAP"] = "NODE_STATE_MAP"
    defaults["IP"] = "IP"
    defaults["OWNER"] = "OWNER"
    defaults["LAUCH_TIME"] = "LAUCH_TIME"
    defaults.update(**kwargs)
    if "AWS_EC2_ACCESS_ID" not in defaults:
        defaults["AWS_EC2_ACCESS_ID"] = create_credentials()
    return INSTANCE.objects.create(**defaults)


def create_outscale_instance_types(**kwargs):
    defaults = {}
    defaults["NAME"] = "NAME"
    defaults["DESCRIPTION"] = "DESCRIPTION"
    defaults.update(**kwargs)
    return OUTSCALE_INSTANCE_TYPES.objects.create(**defaults)


def create_regions(**kwargs):
    defaults = {}
    defaults["NAME"] = "NAME"
    defaults["DESCRIPTION"] = "DESCRIPTION"
    defaults.update(**kwargs)
    return REGIONS.objects.create(**defaults)


class CREDENTIALSViewTest(unittest.TestCase):
    '''
    Tests for CREDENTIALS
    '''
    def setUp(self):
        self.client = Client()

    def test_list_credentials(self):
        url = reverse('amazon_credentials_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_credentials(self):
        url = reverse('amazon_credentials_create')
        data = {
            "AWS_EC2_ACCESS_ID": "AWS_EC2_ACCESS_ID",
            "AWS_EC2_SECRET_KEY": "AWS_EC2_SECRET_KEY",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_credentials(self):
        credentials = create_credentials()
        url = reverse('amazon_credentials_detail', args=[credentials.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_credentials(self):
        credentials = create_credentials()
        data = {
            "AWS_EC2_ACCESS_ID": "AWS_EC2_ACCESS_ID",
            "AWS_EC2_SECRET_KEY": "AWS_EC2_SECRET_KEY",
        }
        url = reverse('amazon_credentials_update', args=[credentials.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class INSTANCEViewTest(unittest.TestCase):
    '''
    Tests for INSTANCE
    '''
    def setUp(self):
        self.client = Client()

    def test_list_instance(self):
        url = reverse('amazon_instance_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_instance(self):
        url = reverse('amazon_instance_create')
        data = {
            "AWS_REGION": "AWS_REGION",
            "AMI_ID": "AMI_ID",
            "SIZE_ID": "SIZE_ID",
            "NAME": "NAME",
            "NODE_STATE_MAP": "NODE_STATE_MAP",
            "IP": "IP",
            "OWNER": "OWNER",
            "LAUCH_TIME": "LAUCH_TIME",
            "AWS_EC2_ACCESS_ID": create_credentials().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_instance(self):
        instance = create_instance()
        url = reverse('amazon_instance_detail', args=[instance.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_instance(self):
        instance = create_instance()
        data = {
            "AWS_REGION": "AWS_REGION",
            "AMI_ID": "AMI_ID",
            "SIZE_ID": "SIZE_ID",
            "NAME": "NAME",
            "NODE_STATE_MAP": "NODE_STATE_MAP",
            "IP": "IP",
            "OWNER": "OWNER",
            "LAUCH_TIME": "LAUCH_TIME",
            "AWS_EC2_ACCESS_ID": create_credentials().pk,
        }
        url = reverse('amazon_instance_update', args=[instance.pk,])

        response = self.client.post(url, data)

        self.assertEqual(response.status_code, 302)


class OUTSCALE_INSTANCE_TYPESViewTest(unittest.TestCase):
    '''
    Tests for OUTSCALE_INSTANCE_TYPES
    '''
    def setUp(self):
        self.client = Client()

    def test_list_outscale_instance_types(self):
        url = reverse('amazon_outscale_instance_types_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_outscale_instance_types(self):
        url = reverse('amazon_outscale_instance_types_create')
        data = {
            "NAME": "NAME",
            "DESCRIPTION": "DESCRIPTION",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_outscale_instance_types(self):
        outscale_instance_types = create_outscale_instance_types()
        url = reverse('amazon_outscale_instance_types_detail', args=[outscale_instance_types.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_outscale_instance_types(self):
        outscale_instance_types = create_outscale_instance_types()
        data = {
            "NAME": "NAME",
            "DESCRIPTION": "DESCRIPTION",
        }
        url = reverse('amazon_outscale_instance_types_update', args=[outscale_instance_types.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class REGIONSViewTest(unittest.TestCase):
    '''
    Tests for REGIONS
    '''
    def setUp(self):
        self.client = Client()

    def test_list_regions(self):
        url = reverse('amazon_regions_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_regions(self):
        url = reverse('amazon_regions_create')
        data = {
            "NAME": "NAME",
            "DESCRIPTION": "DESCRIPTION",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_regions(self):
        regions = create_regions()
        url = reverse('amazon_regions_detail', args=[regions.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_regions(self):
        regions = create_regions()
        data = {
            "NAME": "NAME",
            "DESCRIPTION": "DESCRIPTION",
        }
        url = reverse('amazon_regions_update', args=[regions.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


