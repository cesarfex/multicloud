from . import models

from rest_framework import serializers


class CREDENTIALSSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.CREDENTIALS
        fields = (
            'id',
            'chave',
            'nome',
        )


class INSTANCESerializer(serializers.ModelSerializer):

    class Meta:
        model = models.INSTANCE
        fields = (
            'AWS_REGION',
            'AMI_ID',
            'SIZE_ID',
            'NAME',
            'NODE_STATE_MAP',
            'IP',
            'OWNER',
            'LAUCH_TIME',
            'LAST_REBOOT',
        )


class VOLUMESSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.VOLUMES
        fields = (
            'id',
            'VOLUMEID',
            'SIZE',
            'NAME',
            'TYPE',
            'CREATE_DATE',
            'STATUS'
        )


class KEYSSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.KEYS
        fields = (
            'id',
            'NAME',
        )


class IMAGESSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.IMAGES
        fields = (
            'id',
            'NAME',
        )

class SNAPSHOTSSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SNAPSHOTS
        fields = (
            'id',
            'NAME',
        )