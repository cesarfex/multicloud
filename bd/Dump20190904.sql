CREATE DATABASE  IF NOT EXISTS `amazon` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `amazon`;
-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: 10.12.75.120    Database: amazon
-- ------------------------------------------------------
-- Server version	5.5.60-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `amazon_credentials`
--

DROP TABLE IF EXISTS `amazon_credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_credentials` (
  `id` varchar(45) DEFAULT NULL,
  `chave` varchar(45) DEFAULT NULL,
  `nome` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amazon_credentials`
--

LOCK TABLES `amazon_credentials` WRITE;
/*!40000 ALTER TABLE `amazon_credentials` DISABLE KEYS */;
INSERT INTO `amazon_credentials` VALUES ('AKIAZMQ5T3NFDZN2OGOO','Aj90GltE2yGdplPGbnVjpmGtdTD/6hdyjy7AUsvC','Funchal');
/*!40000 ALTER TABLE `amazon_credentials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amazon_historicalinstance`
--

DROP TABLE IF EXISTS `amazon_historicalinstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_historicalinstance` (
  `AWS_REGION` varchar(20) DEFAULT NULL,
  `AMI_ID` varchar(20) DEFAULT NULL,
  `SIZE_ID` varchar(20) DEFAULT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `NODE_STATE_MAP` varchar(20) DEFAULT NULL,
  `IP` varchar(45) DEFAULT NULL,
  `LAUCH_TIME` varchar(45) DEFAULT NULL,
  `OWNER` varchar(45) DEFAULT NULL,
  `AWS_EC2_ACCESS_ID_id` varchar(45) DEFAULT NULL,
  `history_date` datetime DEFAULT NULL,
  `history_change_reason` datetime DEFAULT NULL,
  `history_type` varchar(40) DEFAULT NULL,
  `history_user_id` varchar(40) DEFAULT NULL,
  `history_id` int(11) DEFAULT NULL,
  `NODE_ID` varchar(45) DEFAULT NULL,
  `LAST_REBOOT` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amazon_historicalinstance`
--

LOCK TABLES `amazon_historicalinstance` WRITE;
/*!40000 ALTER TABLE `amazon_historicalinstance` DISABLE KEYS */;
INSERT INTO `amazon_historicalinstance` VALUES ('us-west-1','ami-013e2b5127d181b0','m1.small','teste','','','2019-08-21 12:59:42.642593','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 14:06:13','0000-00-00 00:00:00','+',NULL,NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste','Running','3.87.129.86','2019-08-21 13:14:27.520578','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 14:06:13','0000-00-00 00:00:00','+',NULL,NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste123','','','2019-08-21 14:32:16.103913','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 14:32:17',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste123','','','2019-08-21 14:32:16.103913','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 14:35:45',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste','Running','3.87.129.86','2019-08-21 13:14:27.520578','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 14:35:57',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.small','teste','','','2019-08-21 12:59:42.642593','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 14:35:57',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste','','','2019-08-21 14:36:47.564057','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 14:36:48',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste','','','2019-08-21 15:35:56.128293','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 15:35:57',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste','','','2019-08-21 15:35:56.128293','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 15:36:41',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste','','','2019-08-21 14:36:47.564057','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 15:36:41',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste','','','2019-08-21 15:37:41.026331','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 15:37:41',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','teste','','','2019-08-21 15:41:46.440882','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 15:41:47',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-2','ami-ff9c5394','m1.medium','teste','','','2019-08-21 15:43:52.109431','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 15:43:53',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-2','ami-013e2b5127d181b0','m1.medium','teste','','','2019-08-21 15:44:37.855335','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 15:44:38',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-2','ami-013e2b5127d181b0','m1.small','teste123','','','2019-08-21 15:53:17.051197','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 15:53:18',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-2','ami-ff9c5394','m1.small','teste123','','','2019-08-21 15:54:59.449152','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 15:55:00',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.small','teste111','','','2019-08-21 15:55:52.573860','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 15:55:53',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','teste123123','','','2019-08-21 16:12:00.240580','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 16:12:01',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','teste123123','','','2019-08-21 17:39:54.948589','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 17:39:56',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.small','teste123123','','','2019-08-21 17:44:32.514921','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 17:44:33',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','Teste Mario','','','2019-08-21 17:48:00.720713','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 17:48:01',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-2','ami-ff9c5394','m1.small','Teste Mario','','','2019-08-21 17:52:20.197822','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 17:52:21',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-2','ami-ff9c5394','m1.medium','Teste Mario','','','2019-08-21 17:53:48.935452','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 17:53:49',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','Teste Mario','','','2019-08-21 17:54:36.844319','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 17:54:37',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.medium','Teste Mario','','','2019-08-21 17:56:43.635888','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 17:56:51',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','Teste Mario','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:00:29',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','Teste Mario','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:01:02',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.medium','Teste Mario','','','2019-08-21 17:56:43.635888','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:01:02',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-2','ami-ff9c5394','m1.medium','Teste Mario','','','2019-08-21 17:53:48.935452','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:01:02',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.small','teste123123','','','2019-08-21 17:44:32.514921','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:01:02',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.small','teste111','','','2019-08-21 15:55:52.573860','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:01:02',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-2','ami-ff9c5394','m1.small','teste123','','','2019-08-21 15:54:59.449152','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:01:02',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste1','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:01:19',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste123','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:02:52',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','teste','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:05:57',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste123123','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:08:32',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.small','teste123123','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:09:30',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','Teste Mario','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:09:48',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.medium','Teste Mario','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:10:24',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','teste111','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:10:42',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-2','ami-ff9c5394','m1.medium','teste111','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:20:26',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-2','ami-ff9c5394','m1.medium','teste','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:20:46',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','sss','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:22:11',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','sss','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:25:17',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-2','ami-013e2b5127d181b0','m1.medium','sdasdasd','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:26:06',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','ssss','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:27:03',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.medium','ssss','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:27:19',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','ssss','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:29:47',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','teste111','','','2019-08-21 18:30:35.794898','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:30:37',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','teste111','','','2019-08-21 18:30:35.794898','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:31:04',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','ssss','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:31:04',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-2','ami-013e2b5127d181b0','m1.medium','sdasdasd','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:31:04',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','sss','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:31:04',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-2','ami-ff9c5394','m1.medium','teste','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:31:04',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-2','ami-ff9c5394','m1.medium','teste111','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:31:04',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.medium','Teste Mario','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:31:04',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.small','teste123123','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:31:04',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','teste','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:31:04',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste123','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:31:04',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste1','','','','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:31:04',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste','Running','3.90.212.195','2019-08-21 18:32:56.272091','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:32:56',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','teste','Running','3.90.212.195','2019-08-21 18:32:56.272091','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 18:36:40',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste','','','2019-08-21 21:09:27.349166','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 21:09:28',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','teste','reboot','','2019-08-21 21:09:27.349166','','AKIAZMQ5T3NFDZN2OGOO','2019-08-21 21:13:18',NULL,'~','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste123','Running','54.196.214.138','2019-08-22 11:44:13.016155','','AKIAZMQ5T3NFDZN2OGOO','2019-08-22 11:44:13',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.medium','teste111','Running','54.167.83.104','2019-08-22 11:47:17.215744','','AKIAZMQ5T3NFDZN2OGOO','2019-08-22 11:47:17',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.medium','teste111','Running','54.167.83.104','2019-08-22 11:47:17.215744','','AKIAZMQ5T3NFDZN2OGOO','2019-08-22 11:51:52',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste123','Running','54.196.214.138','2019-08-22 11:44:13.016155','','AKIAZMQ5T3NFDZN2OGOO','2019-08-22 11:51:52',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','teste','reboot','','2019-08-21 21:09:27.349166','','AKIAZMQ5T3NFDZN2OGOO','2019-08-22 11:51:52',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.medium','teste','reboot','3.90.212.195','2019-08-21 18:32:56.272091','','AKIAZMQ5T3NFDZN2OGOO','2019-08-22 11:51:52',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste123','Running','54.242.118.124','2019-08-22 11:53:04.308234','','AKIAZMQ5T3NFDZN2OGOO','2019-08-22 11:53:04',NULL,'+','1',NULL,NULL,NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste111','Running','54.226.100.172','2019-08-22 12:01:42.594958','','AKIAZMQ5T3NFDZN2OGOO','2019-08-22 12:01:43',NULL,'+','1',NULL,'i-011caaf73e8829545',NULL,NULL),('us-west-1','ami-ff9c5394','m1.small','teste123','Running','54.242.118.124','2019-08-22 11:53:04.308234','','AKIAZMQ5T3NFDZN2OGOO','2019-08-22 12:02:04',NULL,'-','1',NULL,NULL,NULL,NULL),('us-west-1','ami-013e2b5127d181b0','m1.medium','teste123123','Running','3.87.144.48','2019-08-22 14:14:26.258823','','AKIAZMQ5T3NFDZN2OGOO','2019-08-22 14:14:26',NULL,'+','1',NULL,'i-09c37a178fa8434d8',NULL,NULL);
/*!40000 ALTER TABLE `amazon_historicalinstance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amazon_images`
--

DROP TABLE IF EXISTS `amazon_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) DEFAULT NULL,
  `IMAGE_ID` varchar(45) DEFAULT NULL,
  `NODE_ID_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amazon_images`
--

LOCK TABLES `amazon_images` WRITE;
/*!40000 ALTER TABLE `amazon_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `amazon_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amazon_instance`
--

DROP TABLE IF EXISTS `amazon_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_instance` (
  `AWS_REGION` varchar(20) DEFAULT NULL,
  `AMI_ID` varchar(20) DEFAULT NULL,
  `SIZE_ID` varchar(20) DEFAULT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `NODE_STATE_MAP` varchar(20) DEFAULT NULL,
  `IP` varchar(45) DEFAULT NULL,
  `LAUCH_TIME` varchar(45) DEFAULT NULL,
  `OWNER` varchar(45) DEFAULT NULL,
  `AWS_EC2_ACCESS_ID_id` varchar(45) DEFAULT NULL,
  `NODE_ID` varchar(30) DEFAULT NULL,
  `LAST_REBOOT` varchar(45) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `KEY_ID_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amazon_instance`
--

LOCK TABLES `amazon_instance` WRITE;
/*!40000 ALTER TABLE `amazon_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `amazon_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amazon_keys`
--

DROP TABLE IF EXISTS `amazon_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) DEFAULT NULL,
  `FINGERPRINT` varchar(45) DEFAULT NULL,
  `AWS_EC2_ACCESS_ID_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amazon_keys`
--

LOCK TABLES `amazon_keys` WRITE;
/*!40000 ALTER TABLE `amazon_keys` DISABLE KEYS */;
INSERT INTO `amazon_keys` VALUES (35,'Mario','7f:ef:b1:36:76:aa:c3:4f:77:e1:7c:17:b0:86:b0:','AKIAZMQ5T3NFDZN2OGOO');
/*!40000 ALTER TABLE `amazon_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amazon_outscale_instance_types`
--

DROP TABLE IF EXISTS `amazon_outscale_instance_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_outscale_instance_types` (
  `NAME` varchar(20) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amazon_outscale_instance_types`
--

LOCK TABLES `amazon_outscale_instance_types` WRITE;
/*!40000 ALTER TABLE `amazon_outscale_instance_types` DISABLE KEYS */;
INSERT INTO `amazon_outscale_instance_types` VALUES ('m1.small','Arquitetura : 64 bits \r\nVcpu : 1  \r\nMemória : 1,7 \r\nArmazenamento : 160 Gb \r\nPerfomance de rede : baixa',1);
/*!40000 ALTER TABLE `amazon_outscale_instance_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amazon_regioes`
--

DROP TABLE IF EXISTS `amazon_regioes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_regioes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deleted` int(11) DEFAULT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amazon_regioes`
--

LOCK TABLES `amazon_regioes` WRITE;
/*!40000 ALTER TABLE `amazon_regioes` DISABLE KEYS */;
INSERT INTO `amazon_regioes` VALUES (1,0,'BR','Brasil');
/*!40000 ALTER TABLE `amazon_regioes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amazon_regions`
--

DROP TABLE IF EXISTS `amazon_regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_regions` (
  `NAME` varchar(20) DEFAULT NULL,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amazon_regions`
--

LOCK TABLES `amazon_regions` WRITE;
/*!40000 ALTER TABLE `amazon_regions` DISABLE KEYS */;
INSERT INTO `amazon_regions` VALUES ('us-west-2','Leste dos EUA (Ohio)',2),('us-west-1','teste\r\n\r\n\r\nteste',69);
/*!40000 ALTER TABLE `amazon_regions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amazon_snapshots`
--

DROP TABLE IF EXISTS `amazon_snapshots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_snapshots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) DEFAULT NULL,
  `SNAPSHOT_ID` varchar(45) DEFAULT NULL,
  `NODE_ID_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amazon_snapshots`
--

LOCK TABLES `amazon_snapshots` WRITE;
/*!40000 ALTER TABLE `amazon_snapshots` DISABLE KEYS */;
/*!40000 ALTER TABLE `amazon_snapshots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amazon_volumes`
--

DROP TABLE IF EXISTS `amazon_volumes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_volumes` (
  `VOLUMEID` varchar(40) DEFAULT NULL,
  `SIZE` varchar(45) DEFAULT NULL,
  `AWS_REGION` varchar(45) DEFAULT NULL,
  `NAME` varchar(45) DEFAULT NULL,
  `TYPE` varchar(45) DEFAULT NULL,
  `SNAPSHOT` varchar(45) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `STATUS` varchar(45) DEFAULT NULL,
  `IOPS` varchar(45) DEFAULT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `AWS_EC2_ACCESS_ID_id` varchar(45) DEFAULT NULL,
  `NODE_ID_id` varchar(45) DEFAULT NULL,
  `DEVICE` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amazon_volumes`
--

LOCK TABLES `amazon_volumes` WRITE;
/*!40000 ALTER TABLE `amazon_volumes` DISABLE KEYS */;
/*!40000 ALTER TABLE `amazon_volumes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'admin');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
INSERT INTO `auth_group_permissions` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,1,7),(8,1,8),(9,1,9),(10,1,10),(11,1,11),(12,1,12),(13,1,13),(14,1,14),(15,1,15),(16,1,16),(17,1,17),(18,1,18),(19,1,19),(20,1,20),(21,1,21),(22,1,22),(23,1,23),(24,1,24),(25,1,25),(26,1,26),(27,1,27),(28,1,28),(29,1,29),(30,1,30),(31,1,31),(32,1,32),(33,1,33),(34,1,34),(35,1,35),(36,1,36),(37,1,37),(38,1,38),(39,1,39),(40,1,40),(41,1,41),(42,1,42),(43,1,43),(44,1,44),(45,1,45),(46,1,46),(47,1,47),(48,1,48),(49,1,49),(50,1,50),(51,1,51),(52,1,52),(53,1,53),(54,1,54),(55,1,55),(56,1,56),(57,1,57),(58,1,58),(59,1,59),(60,1,60),(61,1,61),(62,1,62),(63,1,63),(64,1,64),(65,1,65),(66,1,66),(67,1,67),(68,1,68),(69,1,69),(70,1,70),(71,1,71),(72,1,72);
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add credentials',7,'add_credentials'),(26,'Can change credentials',7,'change_credentials'),(27,'Can delete credentials',7,'delete_credentials'),(28,'Can view credentials',7,'view_credentials'),(29,'Can add instance',8,'add_instance'),(30,'Can change instance',8,'change_instance'),(31,'Can delete instance',8,'delete_instance'),(32,'Can view instance',8,'view_instance'),(33,'Can add outscal e_instanc e_types',9,'add_outscale_instance_types'),(34,'Can change outscal e_instanc e_types',9,'change_outscale_instance_types'),(35,'Can delete outscal e_instanc e_types',9,'delete_outscale_instance_types'),(36,'Can view outscal e_instanc e_types',9,'view_outscale_instance_types'),(37,'Can add regions',10,'add_regions'),(38,'Can change regions',10,'change_regions'),(39,'Can delete regions',10,'delete_regions'),(40,'Can view regions',10,'view_regions'),(41,'Can add historical instance',11,'add_historicalinstance'),(42,'Can change historical instance',11,'change_historicalinstance'),(43,'Can delete historical instance',11,'delete_historicalinstance'),(44,'Can view historical instance',11,'view_historicalinstance'),(45,'Can add volumes',13,'add_volumes'),(46,'Can change volumes',13,'change_volumes'),(47,'Can delete volumes',13,'delete_volumes'),(48,'Can view volumes',13,'view_volumes'),(49,'Can add bookmark',14,'add_bookmark'),(50,'Can change bookmark',14,'change_bookmark'),(51,'Can delete bookmark',14,'delete_bookmark'),(52,'Can view bookmark',14,'view_bookmark'),(53,'Can add pinned application',15,'add_pinnedapplication'),(54,'Can change pinned application',15,'change_pinnedapplication'),(55,'Can delete pinned application',15,'delete_pinnedapplication'),(56,'Can view pinned application',15,'view_pinnedapplication'),(57,'Can add keys',16,'add_keys'),(58,'Can change keys',16,'change_keys'),(59,'Can delete keys',16,'delete_keys'),(60,'Can view keys',16,'view_keys'),(61,'Can add user dashboard module',17,'add_userdashboardmodule'),(62,'Can change user dashboard module',17,'change_userdashboardmodule'),(63,'Can delete user dashboard module',17,'delete_userdashboardmodule'),(64,'Can view user dashboard module',17,'view_userdashboardmodule'),(65,'Can add images',18,'add_images'),(66,'Can change images',18,'change_images'),(67,'Can delete images',18,'delete_images'),(68,'Can view images',18,'view_images'),(69,'Can add snapshots',19,'add_snapshots'),(70,'Can change snapshots',19,'change_snapshots'),(71,'Can delete snapshots',19,'delete_snapshots'),(72,'Can view snapshots',19,'view_snapshots');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$150000$CQEoy98JXoJw$q65QTRlWXBYQQHZwITamZjtJxsvKJvFfmbnC8ny+ulo=','2019-09-04 12:08:43.263317',1,'admin','','','admin@serpro.gov.br',1,1,'2019-08-13 15:52:26.000000'),(3,'pbkdf2_sha256$150000$J9ZtujcPl7Vc$n9Gi9NBSQVHEryxlov85eSOStgD5eE6CtbBtuv7bUKU=',NULL,1,'teste','','','',0,1,'2019-08-15 12:05:54.000000');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
INSERT INTO `auth_user_groups` VALUES (1,1,1),(2,3,1);
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
INSERT INTO `auth_user_user_permissions` VALUES (2,1,1),(3,1,2),(4,1,3),(5,1,4),(6,1,5),(7,1,6),(8,1,7),(9,1,8),(10,1,9),(11,1,10),(12,1,11),(13,1,12),(14,1,13),(15,1,14),(16,1,15),(17,1,16),(18,1,17),(19,1,18),(20,1,19),(21,1,20),(22,1,21),(23,1,22),(24,1,23),(25,1,24),(26,1,25),(27,1,26),(28,1,27),(29,1,28),(30,1,29),(1,1,30),(31,1,31),(32,1,32),(33,1,33),(34,1,34),(35,1,35),(36,1,36),(37,1,37),(38,1,38),(39,1,39),(40,1,40),(41,1,41),(42,1,42),(43,1,43),(44,1,44),(45,1,45),(46,1,46),(47,1,47),(48,1,48),(49,1,49),(50,1,50),(51,1,51),(52,1,52),(53,1,53),(54,1,54),(55,1,55),(56,1,56),(57,1,57),(58,1,58),(59,1,59),(60,1,60),(61,1,61),(62,1,62),(63,1,63),(64,1,64),(65,1,65),(66,1,66),(67,1,67),(68,1,68),(69,1,69),(70,1,70),(71,1,71),(72,1,72),(73,3,1),(74,3,2),(75,3,3),(76,3,4),(77,3,5),(78,3,6),(79,3,7),(80,3,8),(81,3,9),(82,3,10),(83,3,11),(84,3,12),(85,3,13),(86,3,14),(87,3,15),(88,3,16),(89,3,17),(90,3,18),(91,3,19),(92,3,20),(93,3,21),(94,3,22),(95,3,23),(96,3,24),(97,3,25),(98,3,26),(99,3,27),(100,3,28),(101,3,29),(102,3,30),(103,3,31),(104,3,32),(105,3,33),(106,3,34),(107,3,35),(108,3,36),(109,3,37),(110,3,38),(111,3,39),(112,3,40),(113,3,41),(114,3,42),(115,3,43),(116,3,44),(117,3,45),(118,3,46),(119,3,47),(120,3,48),(121,3,49),(122,3,50),(123,3,51),(124,3,52),(125,3,53),(126,3,54),(127,3,55),(128,3,56),(129,3,57),(130,3,58),(131,3,59),(132,3,60),(133,3,61),(134,3,62),(135,3,63),(136,3,64),(137,3,65),(138,3,66),(139,3,67),(140,3,68),(141,3,69),(142,3,70),(143,3,71),(144,3,72);
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_userdashboardmodule`
--

DROP TABLE IF EXISTS `dashboard_userdashboardmodule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_userdashboardmodule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `app_label` varchar(255) DEFAULT NULL,
  `user` int(10) unsigned NOT NULL,
  `column` int(10) unsigned NOT NULL,
  `order` int(11) NOT NULL,
  `settings` longtext NOT NULL,
  `children` longtext NOT NULL,
  `collapsed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_userdashboardmodule`
--

LOCK TABLES `dashboard_userdashboardmodule` WRITE;
/*!40000 ALTER TABLE `dashboard_userdashboardmodule` DISABLE KEYS */;
INSERT INTO `dashboard_userdashboardmodule` VALUES (7,'Aplicações','jet.dashboard.modules.ModelList','amazon',1,0,0,'{\"models\": [\"amazon.*\"], \"exclude\": null}','',0),(8,'Ações recentes','jet.dashboard.modules.RecentActions','amazon',1,1,0,'{\"limit\": 10}','',0),(9,'Application models','jet.dashboard.modules.ModelList','auth',1,0,0,'{\"models\": [\"auth.*\"], \"exclude\": null}','',0),(10,'Recent Actions','jet.dashboard.modules.RecentActions','auth',1,1,0,'{\"limit\": 10, \"include_list\": [\"auth.*\"], \"exclude_list\": null, \"user\": null}','',0),(11,'Quick links','jet.dashboard.modules.LinkList',NULL,1,0,0,'{\"draggable\": false, \"deletable\": false, \"collapsible\": false, \"layout\": \"inline\"}','[{\"title\": \"Return to site\", \"url\": \"/\"}, {\"title\": \"Alterar senha\", \"url\": \"/admin/password_change/\"}, {\"title\": \"Encerrar sess\\u00e3o\", \"url\": \"/admin/logout/\"}]',0),(12,'Aplicações','jet.dashboard.modules.AppList',NULL,1,1,0,'{\"models\": null, \"exclude\": [\"auth.*\"]}','',0),(13,'Administração','jet.dashboard.modules.AppList',NULL,1,2,0,'{\"models\": [\"auth.*\"], \"exclude\": null}','',0),(14,'Recent Actions','jet.dashboard.modules.RecentActions',NULL,1,0,1,'{\"limit\": 10, \"include_list\": null, \"exclude_list\": null, \"user\": null}','',0);
/*!40000 ALTER TABLE `dashboard_userdashboardmodule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=861 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2019-08-13 16:03:54.282142','0','REGIONS object (0)',1,'[{\"added\": {}}]',10,1),(2,'2019-08-13 16:09:30.745619','0','CREDENTIALS object (0)',1,'[{\"added\": {}}]',7,1),(3,'2019-08-13 16:10:38.136995','0','CREDENTIALS object (0)',1,'[{\"added\": {}}]',7,1),(4,'2019-08-13 16:17:21.474898','3','CREDENTIALS object (3)',1,'[{\"added\": {}}]',7,1),(5,'2019-08-13 16:18:01.142354','3','CREDENTIALS object (3)',3,'',7,1),(6,'2019-08-13 16:18:10.763592','2','CREDENTIALS object (2)',3,'',7,1),(7,'2019-08-13 16:20:22.099666','1','REGIONS object (1)',3,'',10,1),(8,'2019-08-13 16:21:00.442350','2','REGIONS object (2)',1,'[{\"added\": {}}]',10,1),(9,'2019-08-13 16:23:50.454119','1','OUTSCALE_INSTANCE_TYPES object (1)',1,'[{\"added\": {}}]',9,1),(10,'2019-08-13 16:26:35.726175','1','OUTSCALE_INSTANCE_TYPES object (1)',2,'[{\"changed\": {\"fields\": [\"DESCRIPTION\"]}}]',9,1),(11,'2019-08-13 16:26:44.940284','1','OUTSCALE_INSTANCE_TYPES object (1)',2,'[{\"changed\": {\"fields\": [\"DESCRIPTION\"]}}]',9,1),(12,'2019-08-13 16:27:33.857238','1','OUTSCALE_INSTANCE_TYPES object (1)',2,'[{\"changed\": {\"fields\": [\"DESCRIPTION\"]}}]',9,1),(13,'2019-08-13 16:27:58.916327','1','OUTSCALE_INSTANCE_TYPES object (1)',2,'[{\"changed\": {\"fields\": [\"DESCRIPTION\"]}}]',9,1),(14,'2019-08-13 16:28:44.577609','1','OUTSCALE_INSTANCE_TYPES object (1)',2,'[{\"changed\": {\"fields\": [\"DESCRIPTION\"]}}]',9,1),(15,'2019-08-13 16:33:56.410067','2','REGIONS object (2)',2,'[{\"changed\": {\"fields\": [\"DESCRIPTION\"]}}]',10,1),(16,'2019-08-13 16:40:48.780372','1','INSTANCE object (1)',1,'[{\"added\": {}}]',8,1),(17,'2019-08-13 16:42:20.468091','4','CREDENTIALS object (4)',1,'[{\"added\": {}}]',7,1),(18,'2019-08-13 16:42:33.051235','4','CREDENTIALS object (4)',3,'',7,1),(19,'2019-08-13 18:16:48.142432','2','OUTSCALE_INSTANCE_TYPES object (2)',1,'[{\"added\": {}}]',9,1),(20,'2019-08-13 18:17:14.561417','5','CREDENTIALS object (5)',1,'[{\"added\": {}}]',7,1),(21,'2019-08-13 18:17:29.345391','5','CREDENTIALS object (5)',3,'',7,1),(22,'2019-08-13 18:20:57.925007','6','CREDENTIALS object (6)',1,'[{\"added\": {}}]',7,1),(23,'2019-08-13 18:33:27.251534','1','CREDENTIALS object (1)',2,'[]',7,1),(24,'2019-08-13 18:53:42.440050','7','CREDENTIALS object (7)',1,'[{\"added\": {}}]',7,1),(25,'2019-08-14 10:28:37.382566','7','CREDENTIALS object (7)',3,'',7,1),(26,'2019-08-14 10:28:37.426135','6','CREDENTIALS object (6)',3,'',7,1),(27,'2019-08-14 10:29:00.282041','2','OUTSCALE_INSTANCE_TYPES object (2)',3,'',9,1),(28,'2019-08-14 11:55:26.704451','3','REGIONS object (3)',1,'[{\"added\": {}}]',10,1),(29,'2019-08-14 12:00:36.697426','4','REGIONS object (4)',1,'[{\"added\": {}}]',10,1),(30,'2019-08-14 12:43:10.350622','5','REGIONS object (5)',1,'[{\"added\": {}}]',10,1),(31,'2019-08-14 12:58:26.176532','6','REGIONS object (6)',1,'[{\"added\": {}}]',10,1),(32,'2019-08-14 13:00:43.803046','7','REGIONS object (7)',1,'[{\"added\": {}}]',10,1),(33,'2019-08-14 13:01:32.405776','7','REGIONS object (7)',3,'',10,1),(34,'2019-08-14 13:01:32.450170','6','REGIONS object (6)',3,'',10,1),(35,'2019-08-14 13:01:32.493852','5','REGIONS object (5)',3,'',10,1),(36,'2019-08-14 13:01:32.537886','4','REGIONS object (4)',3,'',10,1),(37,'2019-08-14 13:01:32.581943','3','REGIONS object (3)',3,'',10,1),(38,'2019-08-14 13:01:53.818201','8','REGIONS object (8)',1,'[{\"added\": {}}]',10,1),(39,'2019-08-14 13:02:03.859184','8','REGIONS object (8)',2,'[{\"changed\": {\"fields\": [\"NAME\"]}}]',10,1),(40,'2019-08-14 13:21:50.426080','9','REGIONS object (9)',1,'[{\"added\": {}}]',10,1),(41,'2019-08-14 13:24:05.818896','10','REGIONS object (10)',1,'[{\"added\": {}}]',10,1),(42,'2019-08-14 13:24:19.618664','10','REGIONS object (10)',3,'',10,1),(43,'2019-08-14 13:24:19.661076','9','REGIONS object (9)',3,'',10,1),(44,'2019-08-14 13:24:19.704838','8','REGIONS object (8)',3,'',10,1),(45,'2019-08-14 14:08:10.215741','11','REGIONS object (11)',1,'[{\"added\": {}}]',10,1),(46,'2019-08-14 14:08:55.412034','11','REGIONS object (11)',2,'[]',10,1),(47,'2019-08-14 14:12:16.111227','1','CREDENTIALS object (1)',2,'[]',7,1),(48,'2019-08-14 14:34:32.865237','12','REGIONS object (12)',1,'[{\"added\": {}}]',10,1),(49,'2019-08-14 15:47:13.435420','13','REGIONS object (13)',1,'[{\"added\": {}}]',10,1),(50,'2019-08-14 15:49:58.290728','14','REGIONS object (14)',1,'[{\"added\": {}}]',10,1),(51,'2019-08-14 15:51:13.746200','15','REGIONS object (15)',1,'[{\"added\": {}}]',10,1),(52,'2019-08-14 15:53:23.545813','16','REGIONS object (16)',1,'[{\"added\": {}}]',10,1),(53,'2019-08-14 15:54:15.094785','15','REGIONS object (15)',3,'',10,1),(54,'2019-08-14 15:54:15.160940','14','REGIONS object (14)',3,'',10,1),(55,'2019-08-14 15:54:15.223879','13','REGIONS object (13)',3,'',10,1),(56,'2019-08-14 15:54:15.283019','12','REGIONS object (12)',3,'',10,1),(57,'2019-08-14 15:54:15.342412','11','REGIONS object (11)',3,'',10,1),(58,'2019-08-14 15:54:22.924683','16','REGIONS object (16)',2,'[{\"changed\": {\"fields\": [\"NAME\"]}}]',10,1),(59,'2019-08-14 16:00:23.525919','17','REGIONS object (17)',1,'[{\"added\": {}}]',10,1),(60,'2019-08-14 16:01:46.202223','18','REGIONS object (18)',1,'[{\"added\": {}}]',10,1),(61,'2019-08-14 16:04:45.956303','18','REGIONS object (18)',3,'',10,1),(62,'2019-08-14 16:04:46.020153','17','REGIONS object (17)',3,'',10,1),(63,'2019-08-14 16:04:46.080197','16','REGIONS object (16)',3,'',10,1),(64,'2019-08-14 16:04:56.067437','19','REGIONS object (19)',1,'[{\"added\": {}}]',10,1),(65,'2019-08-14 16:06:16.983836','20','REGIONS object (20)',1,'[{\"added\": {}}]',10,1),(66,'2019-08-14 16:07:01.803751','21','REGIONS object (21)',1,'[{\"added\": {}}]',10,1),(67,'2019-08-14 16:15:37.208716','22','REGIONS object (22)',1,'[{\"added\": {}}]',10,1),(68,'2019-08-14 16:17:16.047340','23','REGIONS object (23)',1,'[{\"added\": {}}]',10,1),(69,'2019-08-14 16:18:43.901504','23','REGIONS object (23)',3,'',10,1),(70,'2019-08-14 16:18:43.967846','22','REGIONS object (22)',3,'',10,1),(71,'2019-08-14 16:18:44.030400','21','REGIONS object (21)',3,'',10,1),(72,'2019-08-14 16:18:44.088583','20','REGIONS object (20)',3,'',10,1),(73,'2019-08-14 16:18:44.150283','19','REGIONS object (19)',3,'',10,1),(74,'2019-08-14 17:03:30.693594','24','REGIONS object (24)',1,'[{\"added\": {}}]',10,1),(75,'2019-08-14 17:06:12.439406','24','REGIONS object (24)',2,'[]',10,1),(76,'2019-08-14 17:09:18.806866','25','REGIONS object (25)',1,'[{\"added\": {}}]',10,1),(77,'2019-08-14 17:56:38.140166','26','REGIONS object (26)',1,'[{\"added\": {}}]',10,1),(78,'2019-08-15 12:02:59.380284','2','teste',1,'[{\"added\": {}}]',4,1),(79,'2019-08-15 12:05:27.303358','2','teste',3,'',4,1),(80,'2019-08-15 12:05:54.154752','3','teste',1,'[{\"added\": {}}]',4,1),(81,'2019-08-15 12:57:06.252193','27','REGIONS object (27)',1,'[{\"added\": {}}]',10,1),(82,'2019-08-15 12:59:13.614684','28','REGIONS object (28)',1,'[{\"added\": {}}]',10,1),(83,'2019-08-15 13:03:20.829060','28','REGIONS object (28)',2,'[]',10,1),(84,'2019-08-15 13:11:13.211395','28','REGIONS object (28)',3,'',10,1),(85,'2019-08-15 13:11:13.260450','27','REGIONS object (27)',3,'',10,1),(86,'2019-08-15 13:11:13.303959','26','REGIONS object (26)',3,'',10,1),(87,'2019-08-15 13:11:13.347791','25','REGIONS object (25)',3,'',10,1),(88,'2019-08-15 13:11:13.391794','24','REGIONS object (24)',3,'',10,1),(89,'2019-08-15 13:11:22.520227','29','REGIONS object (29)',1,'[{\"added\": {}}]',10,1),(90,'2019-08-15 13:13:51.039178','30','REGIONS object (30)',1,'[{\"added\": {}}]',10,1),(91,'2019-08-15 13:15:20.840655','31','REGIONS object (31)',1,'[{\"added\": {}}]',10,1),(92,'2019-08-15 13:15:52.053242','32','REGIONS object (32)',1,'[{\"added\": {}}]',10,1),(93,'2019-08-15 13:20:31.266029','33','REGIONS object (33)',1,'[{\"added\": {}}]',10,1),(94,'2019-08-15 13:35:03.458140','34','REGIONS object (34)',1,'[{\"added\": {}}]',10,1),(95,'2019-08-15 13:39:23.644638','35','REGIONS object (35)',1,'[{\"added\": {}}]',10,1),(96,'2019-08-15 13:40:33.181938','35','REGIONS object (35)',2,'[{\"changed\": {\"fields\": [\"DESCRIPTION\"]}}]',10,1),(97,'2019-08-15 13:41:36.684918','35','REGIONS object (35)',2,'[{\"changed\": {\"fields\": [\"NAME\", \"DESCRIPTION\"]}}]',10,1),(98,'2019-08-15 13:43:20.944590','35','REGIONS object (35)',2,'[]',10,1),(99,'2019-08-15 13:54:07.044034','36','REGIONS object (36)',1,'[{\"added\": {}}]',10,1),(100,'2019-08-15 13:58:06.239571','29','REGIONS object (29)',2,'[]',10,1),(101,'2019-08-15 14:12:29.995298','37','REGIONS object (37)',1,'[{\"added\": {}}]',10,1),(102,'2019-08-15 14:30:27.362402','38','REGIONS object (38)',1,'[{\"added\": {}}]',10,1),(103,'2019-08-15 16:25:36.572604','38','REGIONS object (38)',3,'',10,1),(104,'2019-08-15 16:25:36.618241','37','REGIONS object (37)',3,'',10,1),(105,'2019-08-15 16:25:36.661970','36','REGIONS object (36)',3,'',10,1),(106,'2019-08-15 16:25:36.705889','35','REGIONS object (35)',3,'',10,1),(107,'2019-08-15 16:25:36.749940','34','REGIONS object (34)',3,'',10,1),(108,'2019-08-15 16:25:36.793731','33','REGIONS object (33)',3,'',10,1),(109,'2019-08-15 16:27:23.511118','39','REGIONS object (39)',1,'[{\"added\": {}}]',10,1),(110,'2019-08-15 16:39:03.681606','40','REGIONS object (40)',1,'[{\"added\": {}}]',10,1),(111,'2019-08-15 16:41:04.220035','39','REGIONS object (39)',2,'[]',10,1),(112,'2019-08-15 16:42:00.138643','41','REGIONS object (41)',1,'[{\"added\": {}}]',10,1),(113,'2019-08-15 16:42:35.623441','40','REGIONS object (40)',2,'[]',10,1),(114,'2019-08-15 16:44:40.539039','42','REGIONS object (42)',1,'[{\"added\": {}}]',10,1),(115,'2019-08-15 16:46:59.085360','42','REGIONS object (42)',2,'[]',10,1),(116,'2019-08-15 17:49:02.011722','42','REGIONS object (42)',2,'[]',10,1),(117,'2019-08-15 17:53:11.422149','40','REGIONS object (40)',2,'[]',10,1),(118,'2019-08-15 17:57:23.246431','43','REGIONS object (43)',1,'[{\"added\": {}}]',10,1),(119,'2019-08-16 11:37:35.184887','44','REGIONS object (44)',1,'[{\"added\": {}}]',10,1),(120,'2019-08-16 11:37:58.439544','44','REGIONS object (44)',3,'',10,1),(121,'2019-08-16 11:37:58.482680','43','REGIONS object (43)',3,'',10,1),(122,'2019-08-16 11:37:58.523109','42','REGIONS object (42)',3,'',10,1),(123,'2019-08-16 11:37:58.562597','41','REGIONS object (41)',3,'',10,1),(124,'2019-08-16 11:37:58.602976','40','REGIONS object (40)',3,'',10,1),(125,'2019-08-16 11:37:58.642302','39','REGIONS object (39)',3,'',10,1),(126,'2019-08-16 11:37:58.682131','32','REGIONS object (32)',3,'',10,1),(127,'2019-08-16 11:37:58.722364','31','REGIONS object (31)',3,'',10,1),(128,'2019-08-16 11:37:58.762111','30','REGIONS object (30)',3,'',10,1),(129,'2019-08-16 11:37:58.801678','29','REGIONS object (29)',3,'',10,1),(130,'2019-08-16 11:45:27.757815','45','REGIONS object (45)',1,'[{\"added\": {}}]',10,1),(131,'2019-08-16 11:55:41.802371','46','REGIONS object (46)',1,'[{\"added\": {}}]',10,1),(132,'2019-08-16 12:11:14.743655','47','REGIONS object (47)',1,'[{\"added\": {}}]',10,1),(133,'2019-08-16 12:12:52.662721','48','REGIONS object (48)',1,'[{\"added\": {}}]',10,1),(134,'2019-08-16 12:23:11.586782','49','REGIONS object (49)',1,'[{\"added\": {}}]',10,1),(135,'2019-08-16 12:32:03.690158','50','REGIONS object (50)',1,'[{\"added\": {}}]',10,1),(136,'2019-08-16 12:50:44.838473','50','REGIONS object (50)',3,'',10,1),(137,'2019-08-16 12:50:44.884043','49','REGIONS object (49)',3,'',10,1),(138,'2019-08-16 12:50:44.923320','48','REGIONS object (48)',3,'',10,1),(139,'2019-08-16 12:50:44.963680','47','REGIONS object (47)',3,'',10,1),(140,'2019-08-16 12:50:45.003470','46','REGIONS object (46)',3,'',10,1),(141,'2019-08-16 12:50:52.805321','51','REGIONS object (51)',1,'[{\"added\": {}}]',10,1),(142,'2019-08-16 12:52:42.451154','52','REGIONS object (52)',1,'[{\"added\": {}}]',10,1),(143,'2019-08-16 12:53:31.221690','53','REGIONS object (53)',1,'[{\"added\": {}}]',10,1),(144,'2019-08-16 13:15:09.797641','54','REGIONS object (54)',1,'[{\"added\": {}}]',10,1),(145,'2019-08-16 13:17:08.465438','54','REGIONS object (54)',3,'',10,1),(146,'2019-08-16 13:17:08.510309','53','REGIONS object (53)',3,'',10,1),(147,'2019-08-16 13:17:08.550541','52','REGIONS object (52)',3,'',10,1),(148,'2019-08-16 13:17:08.590003','51','REGIONS object (51)',3,'',10,1),(149,'2019-08-16 13:18:39.206524','57','REGIONS object (57)',1,'[{\"added\": {}}]',10,1),(150,'2019-08-16 13:20:35.793888','58','REGIONS object (58)',1,'[{\"added\": {}}]',10,1),(151,'2019-08-16 13:22:10.031747','59','REGIONS object (59)',1,'[{\"added\": {}}]',10,1),(152,'2019-08-16 13:28:04.933897','62','REGIONS object (62)',1,'[{\"added\": {}}]',10,1),(153,'2019-08-16 14:00:55.151532','62','REGIONS object (62)',3,'',10,1),(154,'2019-08-16 14:00:55.194790','59','REGIONS object (59)',3,'',10,1),(155,'2019-08-16 14:00:55.238716','58','REGIONS object (58)',3,'',10,1),(156,'2019-08-16 14:00:55.282757','57','REGIONS object (57)',3,'',10,1),(157,'2019-08-16 14:00:55.326464','45','REGIONS object (45)',3,'',10,1),(158,'2019-08-16 14:01:03.349189','64','REGIONS object (64)',1,'[{\"added\": {}}]',10,1),(159,'2019-08-16 14:02:00.918666','65','REGIONS object (65)',1,'[{\"added\": {}}]',10,1),(160,'2019-08-16 14:02:37.873254','65','REGIONS object (65)',3,'',10,1),(161,'2019-08-16 14:03:46.666803','67','REGIONS object (67)',1,'[{\"added\": {}}]',10,1),(162,'2019-08-16 14:04:29.390029','68','REGIONS object (68)',1,'[{\"added\": {}}]',10,1),(163,'2019-08-16 14:04:45.965635','68','REGIONS object (68)',3,'',10,1),(164,'2019-08-16 14:04:46.008552','67','REGIONS object (67)',3,'',10,1),(165,'2019-08-16 14:04:46.047837','64','REGIONS object (64)',3,'',10,1),(166,'2019-08-16 14:09:27.857964','2','INSTANCE object (2)',1,'[{\"added\": {}}]',8,1),(167,'2019-08-16 14:15:09.755188','2','INSTANCE object (2)',3,'',8,1),(168,'2019-08-16 14:15:20.071242','1','INSTANCE object (1)',2,'[]',8,1),(169,'2019-08-16 14:16:55.027158','3','INSTANCE object (3)',1,'[{\"added\": {}}]',8,1),(170,'2019-08-16 14:21:02.909191','1','INSTANCE object (1)',2,'[]',8,1),(171,'2019-08-16 14:21:41.220265','4','INSTANCE object (4)',1,'[{\"added\": {}}]',8,1),(172,'2019-08-16 14:23:07.785541','5','INSTANCE object (5)',1,'[{\"added\": {}}]',8,1),(173,'2019-08-16 14:23:30.948907','5','INSTANCE object (5)',3,'',8,1),(174,'2019-08-16 14:23:30.994634','4','INSTANCE object (4)',3,'',8,1),(175,'2019-08-16 16:41:42.861325','69','REGIONS object (69)',1,'[{\"added\": {}}]',10,1),(176,'2019-08-16 17:14:35.028353','6','INSTANCE object (6)',1,'[{\"added\": {}}]',8,1),(177,'2019-08-16 17:18:10.520933','6','INSTANCE object (6)',3,'',8,1),(178,'2019-08-16 17:31:06.664673','10','INSTANCE object (10)',1,'[{\"added\": {}}]',8,1),(179,'2019-08-16 17:58:27.005516','11','INSTANCE object (11)',1,'[{\"added\": {}}]',8,1),(180,'2019-08-16 18:23:32.671086','11','INSTANCE object (11)',3,'',8,1),(181,'2019-08-16 18:23:32.715148','10','INSTANCE object (10)',3,'',8,1),(182,'2019-08-19 10:26:47.877412','3','INSTANCE object (3)',3,'',8,1),(183,'2019-08-19 14:08:29.452499','AKIAZMQ5T3NFDZN2OGOO','CREDENTIALS object (AKIAZMQ5T3NFDZN2OGOO)',1,'[{\"added\": {}}]',7,1),(184,'2019-08-19 14:12:54.972467','1','INSTANCE object (1)',2,'[{\"changed\": {\"fields\": [\"AWS_EC2_ACCESS_ID\"]}}]',8,1),(185,'2019-08-19 16:12:26.781830','16','INSTANCE object (16)',1,'[{\"added\": {}}]',8,1),(186,'2019-08-19 16:16:12.803688','17','INSTANCE object (17)',1,'[{\"added\": {}}]',8,1),(187,'2019-08-19 16:18:48.982270','18','INSTANCE object (18)',1,'[{\"added\": {}}]',8,1),(188,'2019-08-19 16:21:36.838285','18','INSTANCE object (18)',3,'',8,1),(189,'2019-08-19 16:21:36.881749','17','INSTANCE object (17)',3,'',8,1),(190,'2019-08-19 16:21:36.922024','16','INSTANCE object (16)',3,'',8,1),(191,'2019-08-19 16:22:35.350429','19','INSTANCE object (19)',1,'[{\"added\": {}}]',8,1),(192,'2019-08-19 16:24:14.322174','20','INSTANCE object (20)',1,'[{\"added\": {}}]',8,1),(193,'2019-08-19 16:29:09.016597','20','INSTANCE object (20)',3,'',8,1),(194,'2019-08-19 16:29:09.060943','19','INSTANCE object (19)',3,'',8,1),(195,'2019-08-19 16:29:55.818344','21','INSTANCE object (21)',1,'[{\"added\": {}}]',8,1),(196,'2019-08-19 16:31:41.109357','21','INSTANCE object (21)',3,'',8,1),(197,'2019-08-19 16:35:06.579855','22','INSTANCE object (22)',1,'[{\"added\": {}}]',8,1),(198,'2019-08-19 16:38:51.410123','22','INSTANCE object (22)',3,'',8,1),(199,'2019-08-19 16:39:13.872475','23','INSTANCE object (23)',1,'[{\"added\": {}}]',8,1),(200,'2019-08-19 17:05:30.986436','28','INSTANCE object (28)',1,'[{\"added\": {}}]',8,1),(201,'2019-08-19 17:08:30.049063','28','INSTANCE object (28)',3,'',8,1),(202,'2019-08-19 17:08:30.094754','23','INSTANCE object (23)',3,'',8,1),(203,'2019-08-19 17:09:50.523219','30','INSTANCE object (30)',1,'[{\"added\": {}}]',8,1),(204,'2019-08-19 17:14:13.066266','32','INSTANCE object (32)',1,'[{\"added\": {}}]',8,1),(205,'2019-08-19 17:15:51.536589','33','INSTANCE object (33)',1,'[{\"added\": {}}]',8,1),(206,'2019-08-19 17:19:29.458706','33','INSTANCE object (33)',3,'',8,1),(207,'2019-08-19 17:19:29.499174','32','INSTANCE object (32)',3,'',8,1),(208,'2019-08-19 17:19:29.543105','30','INSTANCE object (30)',3,'',8,1),(209,'2019-08-19 17:31:17.803861','41','INSTANCE object (41)',1,'[{\"added\": {}}]',8,1),(210,'2019-08-19 17:33:10.945913','42','INSTANCE object (42)',1,'[{\"added\": {}}]',8,1),(211,'2019-08-19 17:34:03.621682','42','INSTANCE object (42)',3,'',8,1),(212,'2019-08-19 17:34:03.665382','41','INSTANCE object (41)',3,'',8,1),(213,'2019-08-19 17:34:19.323319','43','INSTANCE object (43)',1,'[{\"added\": {}}]',8,1),(214,'2019-08-19 17:36:25.318770','44','INSTANCE object (44)',1,'[{\"added\": {}}]',8,1),(215,'2019-08-19 17:37:36.259905','44','INSTANCE object (44)',3,'',8,1),(216,'2019-08-19 17:37:36.301909','43','INSTANCE object (43)',3,'',8,1),(217,'2019-08-19 17:38:07.267766','45','INSTANCE object (45)',1,'[{\"added\": {}}]',8,1),(218,'2019-08-19 17:46:46.103776','45','INSTANCE object (45)',3,'',8,1),(219,'2019-08-19 17:49:13.932214','AKIAZMQ5T3NFDZN2OGOO','CREDENTIALS object (AKIAZMQ5T3NFDZN2OGOO)',2,'[{\"changed\": {\"fields\": [\"chave\"]}}]',7,1),(220,'2019-08-19 17:51:19.586170','AKIAZMQ5T3NFDZN2OGOO','CREDENTIALS object (AKIAZMQ5T3NFDZN2OGOO)',2,'[{\"changed\": {\"fields\": [\"chave\"]}}]',7,1),(221,'2019-08-19 17:51:45.892166','47','INSTANCE object (47)',1,'[{\"added\": {}}]',8,1),(222,'2019-08-19 18:06:11.285288','47','INSTANCE object (47)',3,'',8,1),(223,'2019-08-19 18:09:38.178150','50','INSTANCE object (50)',1,'[{\"added\": {}}]',8,1),(224,'2019-08-19 19:00:42.313659','50','INSTANCE object (50)',3,'',8,1),(225,'2019-08-19 19:02:57.743620','51','INSTANCE object (51)',1,'[{\"added\": {}}]',8,1),(226,'2019-08-20 11:19:19.219045','51','INSTANCE object (51)',3,'',8,1),(227,'2019-08-20 11:19:44.676060','52','INSTANCE object (52)',1,'[{\"added\": {}}]',8,1),(228,'2019-08-20 11:45:47.147478','52','INSTANCE object (52)',3,'',8,1),(229,'2019-08-20 11:49:43.297522','55','INSTANCE object (55)',1,'[{\"added\": {}}]',8,1),(230,'2019-08-20 13:03:15.895102','65','INSTANCE object (65)',1,'[{\"added\": {}}]',8,1),(231,'2019-08-20 13:05:17.771267','65','INSTANCE object (65)',3,'',8,1),(232,'2019-08-20 13:05:17.820304','55','INSTANCE object (55)',3,'',8,1),(233,'2019-08-20 13:08:36.408026','67','INSTANCE object (67)',1,'[{\"added\": {}}]',8,1),(234,'2019-08-20 13:11:14.707634','68','INSTANCE object (68)',1,'[{\"added\": {}}]',8,1),(235,'2019-08-20 13:17:52.119934','68','INSTANCE object (68)',3,'',8,1),(236,'2019-08-20 13:17:52.167897','67','INSTANCE object (67)',3,'',8,1),(237,'2019-08-20 13:18:07.418141','69','INSTANCE object (69)',1,'[{\"added\": {}}]',8,1),(238,'2019-08-20 13:23:23.440629','69','INSTANCE object (69)',2,'[]',8,1),(239,'2019-08-20 16:19:48.353357','70','INSTANCE object (70)',1,'[{\"added\": {}}]',8,1),(240,'2019-08-20 16:21:07.326234','70','INSTANCE object (70)',3,'',8,1),(241,'2019-08-20 16:21:07.369461','69','INSTANCE object (69)',3,'',8,1),(242,'2019-08-20 16:21:28.394642','71','INSTANCE object (71)',1,'[{\"added\": {}}]',8,1),(243,'2019-08-20 16:28:40.937651','72','INSTANCE object (72)',1,'[{\"added\": {}}]',8,1),(244,'2019-08-20 16:32:22.062860','72','INSTANCE object (72)',3,'',8,1),(245,'2019-08-20 16:32:22.108107','71','INSTANCE object (71)',3,'',8,1),(246,'2019-08-20 16:33:31.961500','73','INSTANCE object (73)',1,'[{\"added\": {}}]',8,1),(247,'2019-08-20 17:00:33.123495','73','INSTANCE object (73)',3,'',8,1),(248,'2019-08-20 17:00:33.167223','1','INSTANCE object (1)',3,'',8,1),(249,'2019-08-20 17:02:52.718223','74','INSTANCE object (74)',1,'[{\"added\": {}}]',8,1),(251,'2019-08-20 17:03:43.797066','74','INSTANCE object (74)',3,'',8,1),(252,'2019-08-20 17:05:06.482965','75','INSTANCE object (75)',1,'[{\"added\": {}}]',8,1),(253,'2019-08-20 17:05:34.940043','75','INSTANCE object (75)',3,'',8,1),(254,'2019-08-20 17:26:09.247586','1','admin',2,'[{\"changed\": {\"fields\": [\"user_permissions\"]}}]',4,1),(255,'2019-08-20 17:34:28.569435','76','INSTANCE object (76)',1,'[{\"added\": {}}]',8,1),(256,'2019-08-20 17:35:30.033963','76','INSTANCE object (76)',2,'[]',8,1),(257,'2019-08-20 18:14:13.594843','76','INSTANCE object (76)',3,'',8,1),(258,'2019-08-20 18:19:07.692965','77','INSTANCE object (77)',1,'[{\"added\": {}}]',8,1),(259,'2019-08-20 18:27:56.213019','77','INSTANCE object (77)',2,'[{\"changed\": {\"fields\": [\"SIZE_ID\"]}}]',8,1),(260,'2019-08-20 18:32:48.726204','77','INSTANCE object (77)',3,'',8,1),(261,'2019-08-20 18:39:16.840579','78','INSTANCE object (78)',1,'[{\"added\": {}}]',8,1),(262,'2019-08-20 18:44:16.995689','78','INSTANCE object (78)',3,'',8,1),(263,'2019-08-21 12:59:44.419720','79','INSTANCE object (79)',1,'[{\"added\": {}}]',8,1),(264,'2019-08-21 12:59:47.847515','80','INSTANCE object (80)',1,'[{\"added\": {}}]',8,1),(265,'2019-08-21 13:14:27.964665','80','INSTANCE object (80)',2,'[{\"changed\": {\"fields\": [\"AMI_ID\"]}}]',8,1),(266,'2019-08-21 14:25:16.643236','80','INSTANCE object (80)',3,'',8,1),(267,'2019-08-21 14:25:16.682705','79','INSTANCE object (79)',3,'',8,1),(268,'2019-08-21 14:28:04.506233','80','INSTANCE object (80)',3,'',8,1),(269,'2019-08-21 14:28:04.550950','79','INSTANCE object (79)',3,'',8,1),(270,'2019-08-21 14:28:45.105561','79','INSTANCE object (79)',3,'',8,1),(271,'2019-08-21 14:32:17.523550','81','INSTANCE object (81)',1,'[{\"added\": {}}]',8,1),(272,'2019-08-21 14:32:53.563720','81','INSTANCE object (81)',3,'',8,1),(273,'2019-08-21 14:35:43.776462','81','INSTANCE object (81)',3,'',8,1),(274,'2019-08-21 14:35:55.480768','80','INSTANCE object (80)',3,'',8,1),(275,'2019-08-21 14:35:55.521350','79','INSTANCE object (79)',3,'',8,1),(276,'2019-08-21 14:36:48.701100','82','INSTANCE object (82)',1,'[{\"added\": {}}]',8,1),(277,'2019-08-21 15:35:57.722997','83','INSTANCE object (83)',1,'[{\"added\": {}}]',8,1),(278,'2019-08-21 15:36:39.477252','83','INSTANCE object (83)',3,'',8,1),(279,'2019-08-21 15:36:39.518396','82','INSTANCE object (82)',3,'',8,1),(280,'2019-08-21 15:37:41.975304','84','INSTANCE object (84)',1,'[{\"added\": {}}]',8,1),(281,'2019-08-21 15:41:47.333694','84','INSTANCE object (84)',2,'[{\"changed\": {\"fields\": [\"SIZE_ID\"]}}]',8,1),(282,'2019-08-21 15:43:53.167749','84','INSTANCE object (84)',2,'[{\"changed\": {\"fields\": [\"AWS_REGION\"]}}]',8,1),(283,'2019-08-21 15:44:38.591524','84','INSTANCE object (84)',2,'[{\"changed\": {\"fields\": [\"AMI_ID\"]}}]',8,1),(284,'2019-08-21 15:53:18.422169','84','INSTANCE object (84)',2,'[{\"changed\": {\"fields\": [\"SIZE_ID\", \"AMI_ID\", \"NAME\"]}}]',8,1),(285,'2019-08-21 15:55:00.361978','84','INSTANCE object (84)',2,'[{\"changed\": {\"fields\": [\"AMI_ID\"]}}]',8,1),(286,'2019-08-21 15:55:53.278159','85','INSTANCE object (85)',1,'[{\"added\": {}}]',8,1),(287,'2019-08-21 16:12:01.931726','86','INSTANCE object (86)',1,'[{\"added\": {}}]',8,1),(288,'2019-08-21 17:39:56.680084','86','INSTANCE object (86)',2,'[]',8,1),(289,'2019-08-21 17:44:33.522564','86','INSTANCE object (86)',2,'[{\"changed\": {\"fields\": [\"SIZE_ID\", \"AMI_ID\"]}}]',8,1),(290,'2019-08-21 17:48:02.007523','87','INSTANCE object (87)',1,'[{\"added\": {}}]',8,1),(291,'2019-08-21 17:52:21.280488','87','INSTANCE object (87)',2,'[{\"changed\": {\"fields\": [\"AWS_REGION\"]}}]',8,1),(292,'2019-08-21 17:53:49.667064','87','INSTANCE object (87)',2,'[{\"changed\": {\"fields\": [\"SIZE_ID\"]}}]',8,1),(293,'2019-08-21 17:54:37.572727','88','INSTANCE object (88)',1,'[{\"added\": {}}]',8,1),(294,'2019-08-21 17:56:51.898778','88','INSTANCE object (88)',2,'[{\"changed\": {\"fields\": [\"AMI_ID\"]}}]',8,1),(295,'2019-08-21 18:00:29.318427','89','INSTANCE object (89)',1,'[{\"added\": {}}]',8,1),(296,'2019-08-21 18:00:57.803455','89','INSTANCE object (89)',3,'',8,1),(297,'2019-08-21 18:00:57.846589','88','INSTANCE object (88)',3,'',8,1),(298,'2019-08-21 18:00:57.885829','87','INSTANCE object (87)',3,'',8,1),(299,'2019-08-21 18:00:57.925794','86','INSTANCE object (86)',3,'',8,1),(300,'2019-08-21 18:00:57.966202','85','INSTANCE object (85)',3,'',8,1),(301,'2019-08-21 18:00:58.006236','84','INSTANCE object (84)',3,'',8,1),(302,'2019-08-21 18:01:19.724686','90','INSTANCE object (90)',1,'[{\"added\": {}}]',8,1),(303,'2019-08-21 18:02:52.456959','91','INSTANCE object (91)',1,'[{\"added\": {}}]',8,1),(304,'2019-08-21 18:05:57.878911','92','INSTANCE object (92)',1,'[{\"added\": {}}]',8,1),(305,'2019-08-21 18:08:32.884388','93','INSTANCE object (93)',1,'[{\"added\": {}}]',8,1),(306,'2019-08-21 18:09:30.289113','93','INSTANCE object (93)',2,'[{\"changed\": {\"fields\": [\"AMI_ID\"]}}]',8,1),(307,'2019-08-21 18:09:48.863697','94','INSTANCE object (94)',1,'[{\"added\": {}}]',8,1),(308,'2019-08-21 18:10:24.499505','94','INSTANCE object (94)',2,'[{\"changed\": {\"fields\": [\"AMI_ID\"]}}]',8,1),(309,'2019-08-21 18:10:42.588764','95','INSTANCE object (95)',1,'[{\"added\": {}}]',8,1),(310,'2019-08-21 18:20:26.220350','95','INSTANCE object (95)',2,'[{\"changed\": {\"fields\": [\"AWS_REGION\"]}}]',8,1),(311,'2019-08-21 18:20:46.607614','96','INSTANCE object (96)',1,'[{\"added\": {}}]',8,1),(312,'2019-08-21 18:22:11.987274','97','INSTANCE object (97)',1,'[{\"added\": {}}]',8,1),(313,'2019-08-21 18:25:17.220821','97','INSTANCE object (97)',2,'[]',8,1),(314,'2019-08-21 18:26:06.382728','98','INSTANCE object (98)',1,'[{\"added\": {}}]',8,1),(315,'2019-08-21 18:27:03.914335','99','INSTANCE object (99)',1,'[{\"added\": {}}]',8,1),(316,'2019-08-21 18:27:19.908755','99','INSTANCE object (99)',2,'[{\"changed\": {\"fields\": [\"AMI_ID\"]}}]',8,1),(317,'2019-08-21 18:29:47.497788','99','INSTANCE object (99)',2,'[{\"changed\": {\"fields\": [\"AMI_ID\"]}}]',8,1),(318,'2019-08-21 18:30:37.666132','100','INSTANCE object (100)',1,'[{\"added\": {}}]',8,1),(319,'2019-08-21 18:30:55.737246','100','INSTANCE object (100)',3,'',8,1),(320,'2019-08-21 18:30:55.778399','99','INSTANCE object (99)',3,'',8,1),(321,'2019-08-21 18:30:55.818397','98','INSTANCE object (98)',3,'',8,1),(322,'2019-08-21 18:30:55.858443','97','INSTANCE object (97)',3,'',8,1),(323,'2019-08-21 18:30:55.898248','96','INSTANCE object (96)',3,'',8,1),(324,'2019-08-21 18:30:55.938088','95','INSTANCE object (95)',3,'',8,1),(325,'2019-08-21 18:30:55.977797','94','INSTANCE object (94)',3,'',8,1),(326,'2019-08-21 18:30:56.025682','93','INSTANCE object (93)',3,'',8,1),(327,'2019-08-21 18:30:56.065743','92','INSTANCE object (92)',3,'',8,1),(328,'2019-08-21 18:30:56.105315','91','INSTANCE object (91)',3,'',8,1),(329,'2019-08-21 18:30:56.145429','90','INSTANCE object (90)',3,'',8,1),(330,'2019-08-21 18:32:56.744006','101','INSTANCE object (101)',1,'[{\"added\": {}}]',8,1),(331,'2019-08-21 18:36:40.246954','101','INSTANCE object (101)',2,'[{\"changed\": {\"fields\": [\"SIZE_ID\"]}}]',8,1),(332,'2019-08-21 20:47:24.354619','AKIAZMQ5T3NFDZN2OGOO','CREDENTIALS object (AKIAZMQ5T3NFDZN2OGOO)',1,'[{\"added\": {}}]',7,1),(333,'2019-08-21 20:49:57.285431','0','12222',1,'[{\"added\": {}}]',12,1),(334,'2019-08-21 21:09:28.391599','103','INSTANCE object (103)',1,'[{\"added\": {}}]',8,1),(335,'2019-08-21 21:13:18.905821','103','INSTANCE object (103)',2,'[{\"changed\": {\"fields\": [\"SIZE_ID\"]}}]',8,1),(336,'2019-08-22 11:44:13.509422','104','INSTANCE object (104)',1,'[{\"added\": {}}]',8,1),(337,'2019-08-22 11:47:17.807416','105','INSTANCE object (105)',1,'[{\"added\": {}}]',8,1),(338,'2019-08-22 11:51:48.035781','105','INSTANCE object (105)',3,'',8,1),(339,'2019-08-22 11:51:48.075093','104','INSTANCE object (104)',3,'',8,1),(340,'2019-08-22 11:51:48.115228','103','INSTANCE object (103)',3,'',8,1),(341,'2019-08-22 11:51:48.154767','101','INSTANCE object (101)',3,'',8,1),(342,'2019-08-22 11:53:04.790390','106','INSTANCE object (106)',1,'[{\"added\": {}}]',8,1),(343,'2019-08-22 12:01:43.222620','108','INSTANCE object (108)',1,'[{\"added\": {}}]',8,1),(344,'2019-08-22 12:02:02.601710','106','INSTANCE object (106)',3,'',8,1),(345,'2019-08-22 14:14:26.779115','109','INSTANCE object (109)',1,'[{\"added\": {}}]',8,1),(346,'2019-08-26 12:47:53.219679','1','KEYS object (1)',1,'[{\"added\": {}}]',16,1),(347,'2019-08-26 12:49:26.028330','2','KEYS object (2)',1,'[{\"added\": {}}]',16,1),(348,'2019-08-26 12:51:33.320150','2','KEYS object (2)',3,'',16,1),(349,'2019-08-26 12:51:33.381367','1','KEYS object (1)',3,'',16,1),(350,'2019-08-26 12:52:59.396183','3','KEYS object (3)',1,'[{\"added\": {}}]',16,1),(351,'2019-08-26 12:53:33.681214','4','KEYS object (4)',1,'[{\"added\": {}}]',16,1),(352,'2019-08-26 12:57:57.505763','5','KEYS object (5)',1,'[{\"added\": {}}]',16,1),(353,'2019-08-26 13:21:54.271928','6','KEYS object (6)',1,'[{\"added\": {}}]',16,1),(354,'2019-08-26 13:22:42.529229','5','KEYS object (5)',3,'',16,1),(355,'2019-08-26 13:22:42.571126','4','KEYS object (4)',3,'',16,1),(356,'2019-08-26 13:22:42.610817','3','KEYS object (3)',3,'',16,1),(357,'2019-08-26 13:28:17.570868','7','KEYS object (7)',1,'[{\"added\": {}}]',16,1),(358,'2019-08-26 13:43:28.414564','8','KEYS object (8)',1,'[{\"added\": {}}]',16,1),(359,'2019-08-26 13:47:19.323503','9','KEYS object (9)',1,'[{\"added\": {}}]',16,1),(360,'2019-08-26 13:49:20.555852','8','KEYS object (8)',3,'',16,1),(361,'2019-08-26 13:49:20.597179','7','KEYS object (7)',3,'',16,1),(362,'2019-08-26 13:49:20.636980','6','KEYS object (6)',3,'',16,1),(363,'2019-08-26 13:49:35.961340','10','KEYS object (10)',1,'[{\"added\": {}}]',16,1),(364,'2019-08-26 13:50:16.525973','11','KEYS object (11)',1,'[{\"added\": {}}]',16,1),(365,'2019-08-26 13:51:36.585648','11','KEYS object (11)',3,'',16,1),(366,'2019-08-26 13:51:36.628106','10','KEYS object (10)',3,'',16,1),(367,'2019-08-26 13:51:36.668586','9','KEYS object (9)',3,'',16,1),(368,'2019-08-26 13:51:54.041556','12','KEYS object (12)',1,'[{\"added\": {}}]',16,1),(369,'2019-08-26 13:55:17.678577','13','KEYS object (13)',1,'[{\"added\": {}}]',16,1),(370,'2019-08-26 13:57:56.638475','14','KEYS object (14)',1,'[{\"added\": {}}]',16,1),(371,'2019-08-26 14:01:44.231954','15','KEYS object (15)',1,'[{\"added\": {}}]',16,1),(372,'2019-08-26 14:02:30.816132','15','KEYS object (15)',3,'',16,1),(373,'2019-08-26 14:04:25.730365','14','KEYS object (14)',3,'',16,1),(374,'2019-08-26 14:05:53.615427','13','KEYS object (13)',3,'',16,1),(375,'2019-08-26 14:09:52.913815','12','KEYS object (12)',3,'',16,1),(376,'2019-08-26 14:13:30.823468','16','KEYS object (16)',1,'[{\"added\": {}}]',16,1),(377,'2019-08-26 14:13:45.444756','16','KEYS object (16)',3,'',16,1),(378,'2019-08-26 14:14:07.831048','16','KEYS object (16)',3,'',16,1),(379,'2019-08-26 14:15:51.735756','17','KEYS object (17)',1,'[{\"added\": {}}]',16,1),(380,'2019-08-26 14:16:12.061913','17','KEYS object (17)',3,'',16,1),(381,'2019-08-26 15:44:21.715412','0','VOLUMES object (0)',1,'[{\"added\": {}}]',13,1),(382,'2019-08-26 15:47:19.247863','0','VOLUMES object (0)',3,'',13,1),(383,'2019-08-26 15:47:35.257513','0','VOLUMES object (0)',1,'[{\"added\": {}}]',13,1),(384,'2019-08-26 15:50:13.067488','2','VOLUMES object (2)',1,'[{\"added\": {}}]',13,1),(385,'2019-08-26 15:50:27.626477','2','VOLUMES object (2)',3,'',13,1),(386,'2019-08-26 15:50:27.667505','1','VOLUMES object (1)',3,'',13,1),(387,'2019-08-26 15:51:00.797323','3','VOLUMES object (3)',1,'[{\"added\": {}}]',13,1),(388,'2019-08-26 15:51:18.090940','4','VOLUMES object (4)',1,'[{\"added\": {}}]',13,1),(389,'2019-08-26 15:53:16.279365','4','VOLUMES object (4)',3,'',13,1),(390,'2019-08-26 15:53:16.320794','3','VOLUMES object (3)',3,'',13,1),(391,'2019-08-26 15:54:06.643522','5','VOLUMES object (5)',1,'[{\"added\": {}}]',13,1),(392,'2019-08-26 15:56:04.847540','5','VOLUMES object (5)',3,'',13,1),(393,'2019-08-26 15:56:50.038129','6','VOLUMES object (6)',1,'[{\"added\": {}}]',13,1),(394,'2019-08-26 15:58:37.287353','7','VOLUMES object (7)',1,'[{\"added\": {}}]',13,1),(395,'2019-08-26 16:01:50.292336','8','VOLUMES object (8)',1,'[{\"added\": {}}]',13,1),(396,'2019-08-26 16:04:47.133094','9','VOLUMES object (9)',1,'[{\"added\": {}}]',13,1),(397,'2019-08-26 16:06:11.922010','10','VOLUMES object (10)',1,'[{\"added\": {}}]',13,1),(398,'2019-08-26 16:06:28.193929','9','VOLUMES object (9)',3,'',13,1),(399,'2019-08-26 16:06:28.234821','8','VOLUMES object (8)',3,'',13,1),(400,'2019-08-26 16:06:28.274588','7','VOLUMES object (7)',3,'',13,1),(401,'2019-08-26 16:06:28.314363','6','VOLUMES object (6)',3,'',13,1),(402,'2019-08-26 16:08:05.187072','11','VOLUMES object (11)',1,'[{\"added\": {}}]',13,1),(403,'2019-08-26 16:11:02.015295','11','VOLUMES object (11)',3,'',13,1),(404,'2019-08-26 16:11:02.059064','10','VOLUMES object (10)',3,'',13,1),(405,'2019-08-26 16:14:15.905243','12','VOLUMES object (12)',1,'[{\"added\": {}}]',13,1),(406,'2019-08-26 16:14:33.762149','12','VOLUMES object (12)',3,'',13,1),(407,'2019-08-26 16:15:20.282734','13','VOLUMES object (13)',1,'[{\"added\": {}}]',13,1),(408,'2019-08-26 16:16:20.922789','13','VOLUMES object (13)',3,'',13,1),(409,'2019-08-26 17:57:18.007205','18','KEYS object (18)',1,'[{\"added\": {}}]',16,1),(410,'2019-08-26 18:00:45.705147','19','KEYS object (19)',1,'[{\"added\": {}}]',16,1),(411,'2019-08-26 18:07:38.398616','20','KEYS object (20)',1,'[{\"added\": {}}]',16,1),(412,'2019-08-26 18:12:27.569861','14','VOLUMES object (14)',1,'[{\"added\": {}}]',13,1),(413,'2019-08-26 18:14:22.178491','20','KEYS object (20)',3,'',16,1),(414,'2019-08-26 18:14:22.236252','19','KEYS object (19)',3,'',16,1),(415,'2019-08-26 18:14:22.276372','18','KEYS object (18)',3,'',16,1),(416,'2019-08-26 18:21:24.553855','1','IMAGES object (1)',1,'[{\"added\": {}}]',18,1),(417,'2019-08-26 18:22:23.659319','2','IMAGES object (2)',1,'[{\"added\": {}}]',18,1),(418,'2019-08-26 18:22:52.486703','1','SNAPSHOTS object (1)',1,'[{\"added\": {}}]',19,1),(419,'2019-08-26 18:23:24.088345','3','IMAGES object (3)',1,'[{\"added\": {}}]',18,1),(420,'2019-08-26 18:23:57.522334','4','IMAGES object (4)',1,'[{\"added\": {}}]',18,1),(421,'2019-08-26 18:24:30.267508','2','SNAPSHOTS object (2)',1,'[{\"added\": {}}]',19,1),(422,'2019-08-26 18:38:06.831970','21','KEYS object (21)',1,'[{\"added\": {}}]',16,1),(423,'2019-08-27 11:23:05.790545','5','IMAGES object (5)',1,'[{\"added\": {}}]',18,1),(424,'2019-08-27 11:46:18.493782','118','INSTANCE object (118)',1,'[{\"added\": {}}]',8,1),(425,'2019-08-27 11:47:30.961486','22','KEYS object (22)',1,'[{\"added\": {}}]',16,1),(426,'2019-08-27 11:47:54.828817','3','SNAPSHOTS object (3)',1,'[{\"added\": {}}]',19,1),(427,'2019-08-27 11:48:54.257838','15','VOLUMES object (15)',1,'[{\"added\": {}}]',13,1),(428,'2019-08-27 11:49:58.520453','6','IMAGES object (6)',1,'[{\"added\": {}}]',18,1),(429,'2019-08-27 11:50:54.078017','7','IMAGES object (7)',1,'[{\"added\": {}}]',18,1),(430,'2019-08-27 11:51:28.451826','16','VOLUMES object (16)',1,'[{\"added\": {}}]',13,1),(431,'2019-08-27 11:52:55.494489','119','INSTANCE object (119)',1,'[{\"added\": {}}]',8,1),(432,'2019-08-27 12:11:49.698662','8','IMAGES object (8)',1,'[{\"added\": {}}]',18,1),(433,'2019-08-27 12:13:14.625335','9','IMAGES object (9)',1,'[{\"added\": {}}]',18,1),(434,'2019-08-27 12:13:15.746478','10','IMAGES object (10)',1,'[{\"added\": {}}]',18,1),(435,'2019-08-27 12:16:53.327361','23','KEYS object (23)',1,'[{\"added\": {}}]',16,1),(436,'2019-08-27 12:17:27.407715','11','IMAGES object (11)',1,'[{\"added\": {}}]',18,1),(437,'2019-08-27 12:18:07.658091','4','SNAPSHOTS object (4)',1,'[{\"added\": {}}]',19,1),(438,'2019-08-27 12:19:12.651040','17','VOLUMES object (17)',1,'[{\"added\": {}}]',13,1),(439,'2019-08-27 12:20:05.516964','120','INSTANCE object (120)',1,'[{\"added\": {}}]',8,1),(440,'2019-08-27 12:25:49.048584','5','SNAPSHOTS object (5)',1,'[{\"added\": {}}]',19,1),(441,'2019-08-27 12:28:15.218243','6','SNAPSHOTS object (6)',1,'[{\"added\": {}}]',19,1),(442,'2019-08-27 12:29:27.471340','7','SNAPSHOTS object (7)',1,'[{\"added\": {}}]',19,1),(443,'2019-08-27 12:29:55.288022','8','SNAPSHOTS object (8)',1,'[{\"added\": {}}]',19,1),(444,'2019-08-27 12:30:19.875655','9','SNAPSHOTS object (9)',1,'[{\"added\": {}}]',19,1),(445,'2019-08-27 12:31:09.489905','9','SNAPSHOTS object (9)',3,'',19,1),(446,'2019-08-27 12:31:09.530240','8','SNAPSHOTS object (8)',3,'',19,1),(447,'2019-08-27 12:31:09.569797','7','SNAPSHOTS object (7)',3,'',19,1),(448,'2019-08-27 12:31:09.609631','6','SNAPSHOTS object (6)',3,'',19,1),(449,'2019-08-27 12:31:09.649912','5','SNAPSHOTS object (5)',3,'',19,1),(450,'2019-08-27 12:31:09.689889','4','SNAPSHOTS object (4)',3,'',19,1),(451,'2019-08-27 12:31:09.729763','3','SNAPSHOTS object (3)',3,'',19,1),(452,'2019-08-27 12:31:09.769497','2','SNAPSHOTS object (2)',3,'',19,1),(453,'2019-08-27 12:31:09.809303','1','SNAPSHOTS object (1)',3,'',19,1),(454,'2019-08-27 12:31:20.633621','10','SNAPSHOTS object (10)',1,'[{\"added\": {}}]',19,1),(455,'2019-08-27 12:32:12.968414','18','VOLUMES object (18)',1,'[{\"added\": {}}]',13,1),(456,'2019-08-27 12:33:57.430840','24','KEYS object (24)',1,'[{\"added\": {}}]',16,1),(457,'2019-08-27 12:34:18.699329','25','KEYS object (25)',1,'[{\"added\": {}}]',16,1),(458,'2019-08-27 12:34:44.585044','19','VOLUMES object (19)',1,'[{\"added\": {}}]',13,1),(459,'2019-08-27 12:35:19.952930','11','SNAPSHOTS object (11)',1,'[{\"added\": {}}]',19,1),(460,'2019-08-27 12:37:00.432152','121','INSTANCE object (121)',1,'[{\"added\": {}}]',8,1),(461,'2019-08-27 12:37:25.670831','26','KEYS object (26)',1,'[{\"added\": {}}]',16,1),(462,'2019-08-27 12:44:35.328688','27','KEYS object (27)',1,'[{\"added\": {}}]',16,1),(463,'2019-08-27 12:44:55.333810','20','VOLUMES object (20)',1,'[{\"added\": {}}]',13,1),(464,'2019-08-27 12:45:18.679617','12','SNAPSHOTS object (12)',1,'[{\"added\": {}}]',19,1),(465,'2019-08-27 12:45:43.499466','122','INSTANCE object (122)',1,'[{\"added\": {}}]',8,1),(466,'2019-08-27 12:52:32.767524','11','IMAGES object (11)',3,'',18,1),(467,'2019-08-27 12:52:32.810855','10','IMAGES object (10)',3,'',18,1),(468,'2019-08-27 12:52:32.850737','9','IMAGES object (9)',3,'',18,1),(469,'2019-08-27 12:52:32.891616','8','IMAGES object (8)',3,'',18,1),(470,'2019-08-27 12:52:32.930809','7','IMAGES object (7)',3,'',18,1),(471,'2019-08-27 12:52:32.970547','6','IMAGES object (6)',3,'',18,1),(472,'2019-08-27 12:52:33.010445','5','IMAGES object (5)',3,'',18,1),(473,'2019-08-27 12:52:33.050392','4','IMAGES object (4)',3,'',18,1),(474,'2019-08-27 12:52:33.102103','3','IMAGES object (3)',3,'',18,1),(475,'2019-08-27 12:52:33.141385','2','IMAGES object (2)',3,'',18,1),(476,'2019-08-27 12:52:33.181788','1','IMAGES object (1)',3,'',18,1),(477,'2019-08-27 12:53:07.408405','12','IMAGES object (12)',1,'[{\"added\": {}}]',18,1),(478,'2019-08-27 12:54:18.356467','123','INSTANCE object (123)',1,'[{\"added\": {}}]',8,1),(479,'2019-08-27 12:55:15.462997','13','SNAPSHOTS object (13)',1,'[{\"added\": {}}]',19,1),(480,'2019-08-27 13:04:54.597848','12','IMAGES object (12)',3,'',18,1),(481,'2019-08-27 13:05:06.371666','13','IMAGES object (13)',1,'[{\"added\": {}}]',18,1),(482,'2019-08-27 13:05:32.163488','20','VOLUMES object (20)',3,'',13,1),(483,'2019-08-27 13:05:32.204148','19','VOLUMES object (19)',3,'',13,1),(484,'2019-08-27 13:05:32.244512','18','VOLUMES object (18)',3,'',13,1),(485,'2019-08-27 13:05:32.284291','17','VOLUMES object (17)',3,'',13,1),(486,'2019-08-27 13:05:32.324259','16','VOLUMES object (16)',3,'',13,1),(487,'2019-08-27 13:05:32.364041','15','VOLUMES object (15)',3,'',13,1),(488,'2019-08-27 13:06:02.269539','21','VOLUMES object (21)',1,'[{\"added\": {}}]',13,1),(489,'2019-08-27 13:06:22.037483','13','SNAPSHOTS object (13)',3,'',19,1),(490,'2019-08-27 13:06:22.080815','12','SNAPSHOTS object (12)',3,'',19,1),(491,'2019-08-27 13:06:22.120870','11','SNAPSHOTS object (11)',3,'',19,1),(492,'2019-08-27 13:06:22.160343','10','SNAPSHOTS object (10)',3,'',19,1),(493,'2019-08-27 13:06:32.948039','14','SNAPSHOTS object (14)',1,'[{\"added\": {}}]',19,1),(494,'2019-08-27 13:06:47.629892','27','KEYS object (27)',3,'',16,1),(495,'2019-08-27 13:06:47.670751','26','KEYS object (26)',3,'',16,1),(496,'2019-08-27 13:06:47.710384','25','KEYS object (25)',3,'',16,1),(497,'2019-08-27 13:06:47.750240','24','KEYS object (24)',3,'',16,1),(498,'2019-08-27 13:06:47.790329','23','KEYS object (23)',3,'',16,1),(499,'2019-08-27 13:06:47.829938','22','KEYS object (22)',3,'',16,1),(500,'2019-08-27 13:06:47.869768','21','KEYS object (21)',3,'',16,1),(501,'2019-08-27 13:08:03.175390','28','KEYS object (28)',1,'[{\"added\": {}}]',16,1),(502,'2019-08-27 13:08:57.452703','14','SNAPSHOTS object (14)',3,'',19,1),(503,'2019-08-27 13:09:16.361408','15','SNAPSHOTS object (15)',1,'[{\"added\": {}}]',19,1),(504,'2019-08-27 13:12:26.594057','124','INSTANCE object (124)',1,'[{\"added\": {}}]',8,1),(505,'2019-08-27 13:27:01.245564','14','IMAGES object (14)',1,'[{\"added\": {}}]',18,1),(506,'2019-08-27 13:28:14.867120','14','IMAGES object (14)',3,'',18,1),(507,'2019-08-27 13:28:14.910651','13','IMAGES object (13)',3,'',18,1),(508,'2019-08-27 13:30:03.743721','15','IMAGES object (15)',1,'[{\"added\": {}}]',18,1),(509,'2019-08-27 13:35:05.749265','0','INSTANCE object (0)',1,'[{\"added\": {}}]',8,1),(510,'2019-08-27 13:41:25.735107','16','IMAGES object (16)',1,'[{\"added\": {}}]',18,1),(511,'2019-08-27 13:47:23.721532','17','IMAGES object (17)',1,'[{\"added\": {}}]',18,1),(512,'2019-08-27 14:02:28.493025','17','IMAGES object (17)',3,'',18,1),(513,'2019-08-27 14:02:28.541830','16','IMAGES object (16)',3,'',18,1),(514,'2019-08-27 14:02:28.581702','15','IMAGES object (15)',3,'',18,1),(515,'2019-08-27 14:10:06.945991','18','IMAGES object (18)',1,'[{\"added\": {}}]',18,1),(516,'2019-08-27 14:13:36.730593','19','IMAGES object (19)',1,'[{\"added\": {}}]',18,1),(517,'2019-08-27 14:17:26.288529','20','IMAGES object (20)',1,'[{\"added\": {}}]',18,1),(518,'2019-08-27 14:17:44.301997','20','IMAGES object (20)',3,'',18,1),(519,'2019-08-27 14:17:44.342367','19','IMAGES object (19)',3,'',18,1),(520,'2019-08-27 14:17:44.382530','18','IMAGES object (18)',3,'',18,1),(521,'2019-08-27 14:28:52.700475','21','IMAGES object (21)',1,'[{\"added\": {}}]',18,1),(522,'2019-08-27 15:50:25.018787','22','IMAGES object (22)',1,'[{\"added\": {}}]',18,1),(523,'2019-08-27 15:51:18.249582','22','IMAGES object (22)',3,'',18,1),(524,'2019-08-27 15:51:18.289495','21','IMAGES object (21)',3,'',18,1),(525,'2019-08-27 15:51:33.305988','23','IMAGES object (23)',1,'[{\"added\": {}}]',18,1),(526,'2019-08-27 15:52:08.317818','23','IMAGES object (23)',3,'',18,1),(527,'2019-08-27 15:52:21.638881','24','IMAGES object (24)',1,'[{\"added\": {}}]',18,1),(528,'2019-08-27 15:55:46.141680','3','i-0533c6b76758070ab',1,'[{\"added\": {}}]',8,1),(529,'2019-08-27 15:57:13.116349','25','IMAGES object (25)',1,'[{\"added\": {}}]',18,1),(530,'2019-08-27 16:00:18.864722','26','IMAGES object (26)',1,'[{\"added\": {}}]',18,1),(531,'2019-08-27 16:02:24.660090','26','IMAGES object (26)',3,'',18,1),(532,'2019-08-27 16:02:24.699494','25','IMAGES object (25)',3,'',18,1),(533,'2019-08-27 16:02:40.465643','27','IMAGES object (27)',1,'[{\"added\": {}}]',18,1),(534,'2019-08-27 16:04:32.799873','27','IMAGES object (27)',3,'',18,1),(535,'2019-08-27 16:13:05.339802','28','IMAGES object (28)',1,'[{\"added\": {}}]',18,1),(536,'2019-08-27 16:20:29.188773','29','IMAGES object (29)',1,'[{\"added\": {}}]',18,1),(537,'2019-08-27 16:48:46.172710','29','IMAGES object (29)',3,'',18,1),(538,'2019-08-27 17:16:13.816614','30','IMAGES object (30)',1,'[{\"added\": {}}]',18,1),(539,'2019-08-27 17:18:16.329125','31','IMAGES object (31)',1,'[{\"added\": {}}]',18,1),(540,'2019-08-27 17:24:28.786538','32','IMAGES object (32)',1,'[{\"added\": {}}]',18,1),(541,'2019-08-27 17:27:59.409056','33','IMAGES object (33)',1,'[{\"added\": {}}]',18,1),(542,'2019-08-27 17:45:50.257960','34','IMAGES object (34)',1,'[{\"added\": {}}]',18,1),(543,'2019-08-27 18:00:49.264030','35','IMAGES object (35)',1,'[{\"added\": {}}]',18,1),(544,'2019-08-27 18:10:21.455093','35','IMAGES object (35)',3,'',18,1),(545,'2019-08-27 18:10:41.147432','36','IMAGES object (36)',1,'[{\"added\": {}}]',18,1),(546,'2019-08-27 18:15:25.210087','36','IMAGES object (36)',3,'',18,1),(547,'2019-08-27 18:18:18.374735','37','IMAGES object (37)',1,'[{\"added\": {}}]',18,1),(548,'2019-08-27 18:18:31.000863','37','IMAGES object (37)',3,'',18,1),(549,'2019-08-27 18:20:32.427631','38','IMAGES object (38)',1,'[{\"added\": {}}]',18,1),(550,'2019-08-27 18:20:42.588866','38','IMAGES object (38)',3,'',18,1),(551,'2019-08-27 18:23:37.854326','38','IMAGES object (38)',3,'',18,1),(552,'2019-08-27 18:24:48.406889','38','IMAGES object (38)',3,'',18,1),(553,'2019-08-27 18:25:32.567057','38','IMAGES object (38)',3,'',18,1),(554,'2019-08-27 18:26:26.052503','39','IMAGES object (39)',1,'[{\"added\": {}}]',18,1),(555,'2019-08-27 18:26:38.225429','39','IMAGES object (39)',3,'',18,1),(556,'2019-08-27 18:31:41.875414','39','IMAGES object (39)',3,'',18,1),(557,'2019-08-27 18:35:53.741935','40','IMAGES object (40)',1,'[{\"added\": {}}]',18,1),(558,'2019-08-27 18:37:36.068074','41','IMAGES object (41)',1,'[{\"added\": {}}]',18,1),(559,'2019-08-27 18:38:48.628822','41','IMAGES object (41)',3,'',18,1),(560,'2019-08-27 18:39:21.839113','42','IMAGES object (42)',1,'[{\"added\": {}}]',18,1),(561,'2019-08-27 18:39:37.431660','42','IMAGES object (42)',3,'',18,1),(562,'2019-08-27 18:41:02.049627','43','IMAGES object (43)',1,'[{\"added\": {}}]',18,1),(563,'2019-08-27 18:41:24.002578','43','IMAGES object (43)',3,'',18,1),(564,'2019-08-28 12:15:21.650611','15','SNAPSHOTS object (15)',3,'',19,1),(565,'2019-08-28 12:23:08.368516','16','SNAPSHOTS object (16)',1,'[{\"added\": {}}]',19,1),(566,'2019-08-28 12:25:38.856774','16','SNAPSHOTS object (16)',2,'[]',19,1),(567,'2019-08-28 12:28:51.203019','16','SNAPSHOTS object (16)',2,'[]',19,1),(568,'2019-08-28 12:30:47.719704','16','SNAPSHOTS object (16)',2,'[]',19,1),(569,'2019-08-28 12:31:31.143492','16','SNAPSHOTS object (16)',2,'[]',19,1),(570,'2019-08-28 12:32:24.007348','16','SNAPSHOTS object (16)',2,'[]',19,1),(571,'2019-08-28 12:33:32.725111','16','SNAPSHOTS object (16)',2,'[]',19,1),(572,'2019-08-28 13:06:49.453413','16','SNAPSHOTS object (16)',2,'[{\"changed\": {\"fields\": [\"NODE_ID\"]}}]',19,1),(573,'2019-08-28 13:07:58.523048','16','SNAPSHOTS object (16)',2,'[{\"changed\": {\"fields\": [\"NODE_ID\"]}}]',19,1),(574,'2019-08-28 13:10:30.395083','16','SNAPSHOTS object (16)',2,'[{\"changed\": {\"fields\": [\"NODE_ID\"]}}]',19,1),(575,'2019-08-28 13:11:45.158447','16','SNAPSHOTS object (16)',2,'[{\"changed\": {\"fields\": [\"NODE_ID\"]}}]',19,1),(576,'2019-08-28 13:19:23.275804','16','SNAPSHOTS object (16)',2,'[{\"changed\": {\"fields\": [\"NODE_ID\"]}}]',19,1),(577,'2019-08-28 13:21:05.647755','16','SNAPSHOTS object (16)',2,'[{\"changed\": {\"fields\": [\"NODE_ID\"]}}]',19,1),(578,'2019-08-28 13:21:52.168879','16','SNAPSHOTS object (16)',2,'[{\"changed\": {\"fields\": [\"NODE_ID\"]}}]',19,1),(579,'2019-08-28 13:23:06.464117','16','SNAPSHOTS object (16)',2,'[{\"changed\": {\"fields\": [\"NODE_ID\"]}}]',19,1),(580,'2019-08-28 13:25:33.950262','4','i-0b4f14459207d241f',1,'[{\"added\": {}}]',8,1),(581,'2019-08-28 13:26:11.544614','22','vol-0706e3fc5e0c2b27d',1,'[{\"added\": {}}]',13,1),(582,'2019-08-28 13:26:53.879411','22','vol-0706e3fc5e0c2b27d',3,'',13,1),(583,'2019-08-28 13:33:18.419511','23','vol-0a3a92ecbbc6948f6',1,'[{\"added\": {}}]',13,1),(584,'2019-08-28 13:38:16.783138','23','vol-09ab3f3034f5a0609',2,'[{\"changed\": {\"fields\": [\"NODE_ID\"]}}]',13,1),(585,'2019-08-28 13:42:40.167853','23','vol-0d9f3555811fd3d27',2,'[{\"changed\": {\"fields\": [\"NAME\"]}}]',13,1),(586,'2019-08-28 13:45:03.214378','24','vol-0d9d7c641978ad19e',1,'[{\"added\": {}}]',13,1),(587,'2019-08-28 13:47:40.298862','24','vol-0d9d7c641978ad19e',3,'',13,1),(588,'2019-08-28 13:48:06.192206','25','vol-0dcd14232e6ed006a',1,'[{\"added\": {}}]',13,1),(589,'2019-08-28 13:50:09.643423','25','vol-0dcd14232e6ed006a',3,'',13,1),(590,'2019-08-28 13:50:32.614813','26','vol-0afb4b8b746158eb5',1,'[{\"added\": {}}]',13,1),(591,'2019-08-28 13:54:19.881226','27','vol-024ff93edfe352ee3',1,'[{\"added\": {}}]',13,1),(592,'2019-08-28 13:55:31.208252','27','vol-024ff93edfe352ee3',3,'',13,1),(593,'2019-08-28 13:55:49.002630','28','vol-05dc05bb28aada256',1,'[{\"added\": {}}]',13,1),(594,'2019-08-28 13:56:48.468804','28','vol-05dc05bb28aada256',3,'',13,1),(595,'2019-08-28 13:57:04.961913','29','vol-0cac5a4aac4394343',1,'[{\"added\": {}}]',13,1),(596,'2019-08-28 13:58:01.203818','29','vol-0cac5a4aac4394343',3,'',13,1),(597,'2019-08-28 13:58:18.415708','30','vol-0b5c0b7354799a6c2',1,'[{\"added\": {}}]',13,1),(598,'2019-08-28 13:59:12.471269','30','vol-0b5c0b7354799a6c2',3,'',13,1),(599,'2019-08-28 13:59:31.694531','31','vol-0013983a75436ea46',1,'[{\"added\": {}}]',13,1),(600,'2019-08-28 14:04:00.414655','31','vol-0013983a75436ea46',3,'',13,1),(601,'2019-08-28 14:04:19.094395','32','vol-0484e76b12eb2f43f',1,'[{\"added\": {}}]',13,1),(602,'2019-08-28 14:06:50.288251','32','vol-0484e76b12eb2f43f',3,'',13,1),(603,'2019-08-28 14:07:09.027322','33','vol-076960ddc521cc280',1,'[{\"added\": {}}]',13,1),(604,'2019-08-28 14:10:37.453755','33','vol-076960ddc521cc280',3,'',13,1),(605,'2019-08-28 14:10:55.984861','34','vol-00f1a7df0c326beff',1,'[{\"added\": {}}]',13,1),(606,'2019-08-28 14:12:52.702960','34','vol-00f1a7df0c326beff',3,'',13,1),(607,'2019-08-28 14:13:21.484105','35','vol-0a6d67934586243b4',1,'[{\"added\": {}}]',13,1),(608,'2019-08-28 14:15:01.049018','35','vol-0a6d67934586243b4',3,'',13,1),(609,'2019-08-28 14:15:20.029936','36','vol-010aa2ea2b38f25c9',1,'[{\"added\": {}}]',13,1),(610,'2019-08-28 14:16:22.612959','36','vol-010aa2ea2b38f25c9',3,'',13,1),(611,'2019-08-28 14:16:42.886462','37','vol-0b7449bd416e65b42',1,'[{\"added\": {}}]',13,1),(612,'2019-08-28 14:18:53.213276','37','vol-0b7449bd416e65b42',3,'',13,1),(613,'2019-08-28 15:59:17.704329','38','',1,'[{\"added\": {}}]',13,1),(614,'2019-08-28 16:02:30.830771','39','vol-05bd328bfe1d43665',1,'[{\"added\": {}}]',13,1),(615,'2019-08-28 16:04:34.885010','40','vol-0266b11efec079e1d',1,'[{\"added\": {}}]',13,1),(616,'2019-08-28 16:05:10.146550','40','vol-0266b11efec079e1d',3,'',13,1),(617,'2019-08-28 16:15:05.120242','4','i-0b4f14459207d241f',3,'',8,1),(618,'2019-08-28 16:19:01.632588','5','i-032e84c3271f0ee19',1,'[{\"added\": {}}]',8,1),(619,'2019-08-28 16:50:47.191831','41','vol-02ab18b6d831db2bf',1,'[{\"added\": {}}]',13,1),(620,'2019-08-28 16:52:41.416283','6','i-08932ac15310423a0',1,'[{\"added\": {}}]',8,1),(621,'2019-08-28 17:02:24.020136','42','',1,'[{\"added\": {}}]',13,1),(622,'2019-08-28 17:38:54.056563','43','',1,'[{\"added\": {}}]',13,1),(623,'2019-08-28 17:41:17.461172','44','',1,'[{\"added\": {}}]',13,1),(624,'2019-08-28 17:42:25.349779','45','',1,'[{\"added\": {}}]',13,1),(625,'2019-08-28 17:43:39.940819','45','',3,'',13,1),(626,'2019-08-28 17:43:39.983567','44','',3,'',13,1),(627,'2019-08-28 17:43:40.031207','43','',3,'',13,1),(628,'2019-08-28 17:43:40.071233','42','',3,'',13,1),(629,'2019-08-28 17:43:57.240401','46','',1,'[{\"added\": {}}]',13,1),(630,'2019-08-28 17:46:01.595654','47','',1,'[{\"added\": {}}]',13,1),(631,'2019-08-28 17:51:05.536278','48','',1,'[{\"added\": {}}]',13,1),(632,'2019-08-28 17:52:21.917880','49','',1,'[{\"added\": {}}]',13,1),(633,'2019-08-28 17:54:21.456916','50','',1,'[{\"added\": {}}]',13,1),(634,'2019-08-28 17:56:12.038830','51','',1,'[{\"added\": {}}]',13,1),(635,'2019-08-28 17:57:01.631380','52','',1,'[{\"added\": {}}]',13,1),(636,'2019-08-28 17:58:25.108008','53','',1,'[{\"added\": {}}]',13,1),(637,'2019-08-28 18:00:06.463217','54','',1,'[{\"added\": {}}]',13,1),(638,'2019-08-28 18:04:08.105847','55','',1,'[{\"added\": {}}]',13,1),(639,'2019-08-28 18:06:11.111295','56','',1,'[{\"added\": {}}]',13,1),(640,'2019-08-28 18:28:30.005261','57','vol-05f53bb01e7684cb3',1,'[{\"added\": {}}]',13,1),(641,'2019-08-28 18:29:43.577844','57','vol-05f53bb01e7684cb3',3,'',13,1),(642,'2019-08-28 18:30:05.926159','58','vol-0d44eff295e00bb45',1,'[{\"added\": {}}]',13,1),(643,'2019-08-28 18:37:34.525021','59','vol-05b1add84f37c8758',1,'[{\"added\": {}}]',13,1),(644,'2019-08-28 18:39:18.972483','60','vol-0bc26e7daa5af252f',1,'[{\"added\": {}}]',13,1),(645,'2019-08-28 18:43:53.559162','61','vol-048aa93cced4ce3bf',1,'[{\"added\": {}}]',13,1),(646,'2019-08-28 18:45:24.138581','62','vol-0da7d21f4e3a2f96a',1,'[{\"added\": {}}]',13,1),(647,'2019-08-28 18:46:43.778961','63','vol-05b2505f4942c269f',1,'[{\"added\": {}}]',13,1),(648,'2019-08-28 18:53:44.335092','6','i-08932ac15310423a0',3,'',8,1),(649,'2019-08-28 18:53:44.376167','5','i-032e84c3271f0ee19',3,'',8,1),(650,'2019-08-28 18:55:13.634695','7','i-09a49738f3191964a',1,'[{\"added\": {}}]',8,1),(651,'2019-08-28 18:59:40.406411','64','vol-01c33a888f9baa30f',1,'[{\"added\": {}}]',13,1),(652,'2019-08-28 19:01:25.772710','8','i-068b6f85c799bf8aa',1,'[{\"added\": {}}]',8,1),(653,'2019-08-29 10:24:56.942691','9','i-0383096a08bd76c7c',1,'[{\"added\": {}}]',8,1),(654,'2019-08-29 11:18:58.118397','10','i-05cd2a5f53bbce0f8',1,'[{\"added\": {}}]',8,1),(655,'2019-08-29 11:22:35.860600','11','i-04d76be422763ace4',1,'[{\"added\": {}}]',8,1),(656,'2019-08-29 11:25:36.473090','12','i-067aaeed2b05b506b',1,'[{\"added\": {}}]',8,1),(657,'2019-08-29 12:44:13.490252','13','i-05471e8205dbc42ff',1,'[{\"added\": {}}]',8,1),(658,'2019-08-29 12:48:51.397200','65','',1,'[{\"added\": {}}]',13,1),(659,'2019-08-29 13:30:11.839223','14','i-0aaf6e0fa274f672f',1,'[{\"added\": {}}]',8,1),(660,'2019-08-29 13:34:13.744105','15','i-0493226f045fe486b',1,'[{\"added\": {}}]',8,1),(661,'2019-08-29 13:35:20.019362','16','i-071707cf17ac6d92f',1,'[{\"added\": {}}]',8,1),(662,'2019-08-29 13:37:59.556925','66','vol-08051e970384544ed',1,'[{\"added\": {}}]',13,1),(663,'2019-08-29 13:38:40.858116','67','vol-09810f1b5b7e30797',1,'[{\"added\": {}}]',13,1),(664,'2019-08-29 13:44:23.599303','68','vol-0c2136348a8a0d973',1,'[{\"added\": {}}]',13,1),(665,'2019-08-29 13:45:57.197546','44','IMAGES object (44)',1,'[{\"added\": {}}]',18,1),(666,'2019-08-29 13:53:10.310678','17','i-0e6375d724c383e9d',1,'[{\"added\": {}}]',8,1),(667,'2019-08-29 13:54:18.587639','69','vol-02bad55f3df61d385',1,'[{\"added\": {}}]',13,1),(668,'2019-08-29 13:55:26.053147','18','i-0daaee70b9b75bc77',1,'[{\"added\": {}}]',8,1),(669,'2019-08-29 13:56:47.730497','70','vol-0908c7fd309430b30',1,'[{\"added\": {}}]',13,1),(670,'2019-08-29 14:05:39.341788','70','vol-0908c7fd309430b30',3,'',13,1),(671,'2019-08-29 14:05:39.384816','69','vol-02bad55f3df61d385',3,'',13,1),(672,'2019-08-29 14:10:22.151330','70','vol-0908c7fd309430b30',3,'',13,1),(673,'2019-08-29 14:11:17.004221','69','vol-02bad55f3df61d385',3,'',13,1),(674,'2019-08-29 14:12:40.412647','69','vol-02bad55f3df61d385',3,'',13,1),(675,'2019-08-29 14:14:35.156552','71','vol-06f11c3242c97beb4',1,'[{\"added\": {}}]',13,1),(676,'2019-08-29 14:14:44.872127','71','vol-06f11c3242c97beb4',3,'',13,1),(677,'2019-08-29 14:15:40.178593','72','vol-0fe0f9d1a3484164b',1,'[{\"added\": {}}]',13,1),(678,'2019-08-29 14:15:54.171117','72','vol-0fe0f9d1a3484164b',3,'',13,1),(679,'2019-08-29 14:26:24.111610','73','vol-00baff1e50df48174',1,'[{\"added\": {}}]',13,1),(680,'2019-08-29 14:29:16.120760','45','IMAGES object (45)',1,'[{\"added\": {}}]',18,1),(681,'2019-08-29 14:33:10.799897','46','IMAGES object (46)',1,'[{\"added\": {}}]',18,1),(682,'2019-08-29 14:34:58.856833','47','IMAGES object (47)',1,'[{\"added\": {}}]',18,1),(683,'2019-08-29 14:36:16.617380','47','IMAGES object (47)',3,'',18,1),(684,'2019-08-29 14:36:16.666728','46','IMAGES object (46)',3,'',18,1),(685,'2019-08-29 14:36:16.706528','45','IMAGES object (45)',3,'',18,1),(686,'2019-08-29 14:36:16.746293','44','IMAGES object (44)',3,'',18,1),(687,'2019-08-29 14:36:38.646157','44','IMAGES object (44)',3,'',18,1),(688,'2019-08-29 15:30:47.648602','44','IMAGES object (44)',3,'',18,1),(689,'2019-08-29 15:31:06.939620','48','IMAGES object (48)',1,'[{\"added\": {}}]',18,1),(690,'2019-08-29 15:33:04.329370','49','IMAGES object (49)',1,'[{\"added\": {}}]',18,1),(691,'2019-08-29 15:33:32.429172','50','IMAGES object (50)',1,'[{\"added\": {}}]',18,1),(692,'2019-08-29 15:34:52.236009','51','IMAGES object (51)',1,'[{\"added\": {}}]',18,1),(693,'2019-08-29 15:35:48.555619','52','IMAGES object (52)',1,'[{\"added\": {}}]',18,1),(694,'2019-08-29 15:37:23.607046','17','SNAPSHOTS object (17)',1,'[{\"added\": {}}]',19,1),(695,'2019-08-29 15:39:46.607037','18','SNAPSHOTS object (18)',1,'[{\"added\": {}}]',19,1),(696,'2019-08-29 15:45:30.430388','19','SNAPSHOTS object (19)',1,'[{\"added\": {}}]',19,1),(697,'2019-08-29 15:50:30.189623','73','vol-00baff1e50df48174',3,'',13,1),(698,'2019-08-29 15:51:21.614711','74','vol-0a709941b61e6e234',1,'[{\"added\": {}}]',13,1),(699,'2019-08-29 15:56:06.178162','75','vol-0420e326a93cfe3e1',1,'[{\"added\": {}}]',13,1),(700,'2019-08-29 15:59:10.554378','75','vol-0420e326a93cfe3e1',3,'',13,1),(701,'2019-08-29 15:59:10.597791','74','vol-0a709941b61e6e234',3,'',13,1),(702,'2019-08-29 16:00:30.643658','76','vol-0f550d9450a0742c1',1,'[{\"added\": {}}]',13,1),(703,'2019-08-29 16:02:48.090571','76','vol-0f550d9450a0742c1',3,'',13,1),(704,'2019-08-29 16:03:37.865976','77','vol-036452d64b6158485',1,'[{\"added\": {}}]',13,1),(705,'2019-08-29 16:04:01.205088','77','vol-036452d64b6158485',3,'',13,1),(706,'2019-08-29 16:04:47.640617','78','vol-08b401b1f2df46b6b',1,'[{\"added\": {}}]',13,1),(707,'2019-08-29 16:08:56.698950','78','vol-08b401b1f2df46b6b',3,'',13,1),(708,'2019-08-29 16:09:48.721264','78','vol-08b401b1f2df46b6b',3,'',13,1),(709,'2019-08-29 16:11:03.592041','79','vol-0f1fb872abb1ff623',1,'[{\"added\": {}}]',13,1),(710,'2019-08-29 16:12:22.667834','20','SNAPSHOTS object (20)',1,'[{\"added\": {}}]',19,1),(711,'2019-08-29 16:47:25.796820','21','SNAPSHOTS object (21)',1,'[{\"added\": {}}]',19,1),(712,'2019-08-29 16:49:08.103726','22','SNAPSHOTS object (22)',1,'[{\"added\": {}}]',19,1),(713,'2019-08-29 16:50:41.538518','23','SNAPSHOTS object (23)',1,'[{\"added\": {}}]',19,1),(714,'2019-08-29 16:51:39.212456','24','SNAPSHOTS object (24)',1,'[{\"added\": {}}]',19,1),(715,'2019-08-29 16:52:59.833781','25','SNAPSHOTS object (25)',1,'[{\"added\": {}}]',19,1),(716,'2019-08-29 16:55:35.782170','26','SNAPSHOTS object (26)',1,'[{\"added\": {}}]',19,1),(717,'2019-08-29 16:56:55.385714','27','SNAPSHOTS object (27)',1,'[{\"added\": {}}]',19,1),(718,'2019-08-29 16:58:34.070904','28','SNAPSHOTS object (28)',1,'[{\"added\": {}}]',19,1),(719,'2019-08-29 16:59:16.524288','29','SNAPSHOTS object (29)',1,'[{\"added\": {}}]',19,1),(720,'2019-08-29 17:00:54.973356','30','SNAPSHOTS object (30)',1,'[{\"added\": {}}]',19,1),(721,'2019-08-29 17:05:45.353301','31','SNAPSHOTS object (31)',1,'[{\"added\": {}}]',19,1),(722,'2019-08-29 17:07:12.618756','32','SNAPSHOTS object (32)',1,'[{\"added\": {}}]',19,1),(723,'2019-08-29 17:08:21.589934','33','SNAPSHOTS object (33)',1,'[{\"added\": {}}]',19,1),(724,'2019-08-29 17:09:11.814449','34','SNAPSHOTS object (34)',1,'[{\"added\": {}}]',19,1),(725,'2019-08-29 17:10:03.369593','34','SNAPSHOTS object (34)',3,'',19,1),(726,'2019-08-29 17:10:03.412260','33','SNAPSHOTS object (33)',3,'',19,1),(727,'2019-08-29 17:10:03.451290','32','SNAPSHOTS object (32)',3,'',19,1),(728,'2019-08-29 17:10:03.491710','31','SNAPSHOTS object (31)',3,'',19,1),(729,'2019-08-29 17:10:03.531631','30','SNAPSHOTS object (30)',3,'',19,1),(730,'2019-08-29 17:10:03.571703','29','SNAPSHOTS object (29)',3,'',19,1),(731,'2019-08-29 17:10:03.611455','28','SNAPSHOTS object (28)',3,'',19,1),(732,'2019-08-29 17:10:03.651885','27','SNAPSHOTS object (27)',3,'',19,1),(733,'2019-08-29 17:10:03.691395','26','SNAPSHOTS object (26)',3,'',19,1),(734,'2019-08-29 17:10:03.731375','25','SNAPSHOTS object (25)',3,'',19,1),(735,'2019-08-29 17:10:03.770827','24','SNAPSHOTS object (24)',3,'',19,1),(736,'2019-08-29 17:10:03.810733','23','SNAPSHOTS object (23)',3,'',19,1),(737,'2019-08-29 17:10:03.850663','22','SNAPSHOTS object (22)',3,'',19,1),(738,'2019-08-29 17:10:03.890851','21','SNAPSHOTS object (21)',3,'',19,1),(739,'2019-08-29 17:10:03.930752','20','SNAPSHOTS object (20)',3,'',19,1),(740,'2019-08-29 17:11:15.169806','35','SNAPSHOTS object (35)',1,'[{\"added\": {}}]',19,1),(741,'2019-08-29 17:12:26.818409','36','SNAPSHOTS object (36)',1,'[{\"added\": {}}]',19,1),(742,'2019-08-29 17:13:48.947489','36','SNAPSHOTS object (36)',3,'',19,1),(743,'2019-08-29 17:13:48.987662','35','SNAPSHOTS object (35)',3,'',19,1),(744,'2019-08-29 17:17:02.522747','37','SNAPSHOTS object (37)',1,'[{\"added\": {}}]',19,1),(745,'2019-08-29 17:23:10.761733','37','SNAPSHOTS object (37)',3,'',19,1),(746,'2019-08-29 17:23:58.399757','38','SNAPSHOTS object (38)',1,'[{\"added\": {}}]',19,1),(747,'2019-08-29 17:24:14.995058','38','SNAPSHOTS object (38)',3,'',19,1),(748,'2019-08-29 17:25:55.310192','39','SNAPSHOTS object (39)',1,'[{\"added\": {}}]',19,1),(749,'2019-08-29 17:26:21.930174','39','SNAPSHOTS object (39)',3,'',19,1),(750,'2019-08-29 17:28:26.061536','52','IMAGES object (52)',3,'',18,1),(751,'2019-08-29 17:28:26.102747','51','IMAGES object (51)',3,'',18,1),(752,'2019-08-29 17:30:09.736523','40','SNAPSHOTS object (40)',1,'[{\"added\": {}}]',19,1),(753,'2019-08-29 17:35:03.067621','41','SNAPSHOTS object (41)',1,'[{\"added\": {}}]',19,1),(754,'2019-08-29 17:36:52.032727','42','SNAPSHOTS object (42)',1,'[{\"added\": {}}]',19,1),(755,'2019-08-29 17:42:51.401944','42','SNAPSHOTS object (42)',3,'',19,1),(756,'2019-08-29 17:45:22.008521','43','SNAPSHOTS object (43)',1,'[{\"added\": {}}]',19,1),(757,'2019-08-29 17:45:53.474536','43','SNAPSHOTS object (43)',3,'',19,1),(758,'2019-08-29 17:47:53.633930','44','SNAPSHOTS object (44)',1,'[{\"added\": {}}]',19,1),(759,'2019-08-29 17:49:34.827910','45','SNAPSHOTS object (45)',1,'[{\"added\": {}}]',19,1),(760,'2019-08-29 17:50:51.377624','46','SNAPSHOTS object (46)',1,'[{\"added\": {}}]',19,1),(761,'2019-08-29 17:51:02.491516','46','SNAPSHOTS object (46)',3,'',19,1),(762,'2019-08-29 17:51:02.533051','45','SNAPSHOTS object (45)',3,'',19,1),(763,'2019-08-29 17:51:02.573460','44','SNAPSHOTS object (44)',3,'',19,1),(764,'2019-08-29 17:51:34.750339','47','SNAPSHOTS object (47)',1,'[{\"added\": {}}]',19,1),(765,'2019-08-29 17:51:57.639691','47','SNAPSHOTS object (47)',3,'',19,1),(766,'2019-08-29 17:53:38.087204','53','IMAGES object (53)',1,'[{\"added\": {}}]',18,1),(767,'2019-08-29 17:56:57.084967','54','IMAGES object (54)',1,'[{\"added\": {}}]',18,1),(768,'2019-08-29 17:58:35.097785','55','IMAGES object (55)',1,'[{\"added\": {}}]',18,1),(769,'2019-08-29 18:00:04.239145','48','SNAPSHOTS object (48)',1,'[{\"added\": {}}]',19,1),(770,'2019-08-29 18:00:49.806738','48','SNAPSHOTS object (48)',3,'',19,1),(771,'2019-08-29 18:03:36.410530','56','IMAGES object (56)',1,'[{\"added\": {}}]',18,1),(772,'2019-08-29 18:10:40.888938','57','IMAGES object (57)',1,'[{\"added\": {}}]',18,1),(773,'2019-08-29 18:15:24.703100','58','IMAGES object (58)',1,'[{\"added\": {}}]',18,1),(774,'2019-08-29 18:17:43.657252','59','IMAGES object (59)',1,'[{\"added\": {}}]',18,1),(775,'2019-08-29 18:26:20.749950','60','IMAGES object (60)',1,'[{\"added\": {}}]',18,1),(776,'2019-08-29 18:29:39.079186','61','IMAGES object (61)',1,'[{\"added\": {}}]',18,1),(777,'2019-08-30 10:28:13.742672','29','Funchal-teste',1,'[{\"added\": {}}]',16,1),(778,'2019-08-30 10:29:31.368686','19','i-0bfe88d27376f441e',1,'[{\"added\": {}}]',8,1),(779,'2019-08-30 10:30:39.149041','80','vol-069b6832ddb7d62bc',1,'[{\"added\": {}}]',13,1),(780,'2019-08-30 10:31:36.200276','62','IMAGES object (62)',1,'[{\"added\": {}}]',18,1),(783,'2019-08-30 10:57:59.754849','49','SNAPSHOTS object (49)',1,'[{\"added\": {}}]',19,1),(784,'2019-08-30 11:10:29.501619','20','i-09de80a59ba049ca2',1,'[{\"added\": {}}]',8,1),(785,'2019-08-30 11:11:52.013017','81','vol-00a311e1ab1843852',1,'[{\"added\": {}}]',13,1),(786,'2019-08-30 11:13:19.472732','80','vol-069b6832ddb7d62bc',3,'',13,1),(787,'2019-08-30 11:15:07.459120','50','SNAPSHOTS object (50)',1,'[{\"added\": {}}]',19,1),(788,'2019-08-30 11:16:57.020890','51','SNAPSHOTS object (51)',1,'[{\"added\": {}}]',19,1),(789,'2019-08-30 11:18:07.672329','51','SNAPSHOTS object (51)',3,'',19,1),(790,'2019-08-30 11:19:23.801716','81','vol-00a311e1ab1843852',3,'',13,1),(791,'2019-08-30 11:20:31.123733','20','i-09de80a59ba049ca2',3,'',8,1),(792,'2019-08-30 11:20:51.618250','29','Funchal-teste',3,'',16,1),(793,'2019-08-30 11:21:46.806650','30','Mario',1,'[{\"added\": {}}]',16,1),(794,'2019-08-30 11:22:54.970581','21','i-0a6faaf74c7cfaf0e',1,'[{\"added\": {}}]',8,1),(795,'2019-08-30 11:23:36.257199','82','vol-0d66591fa66aad77b',1,'[{\"added\": {}}]',13,1),(796,'2019-08-30 11:38:32.851627','82','vol-0d66591fa66aad77b',3,'',13,1),(797,'2019-08-30 11:39:44.736739','83','vol-066e3cf04ef423422',1,'[{\"added\": {}}]',13,1),(798,'2019-08-30 11:40:18.028257','63','IMAGES object (63)',1,'[{\"added\": {}}]',18,1),(799,'2019-08-30 11:42:40.659587','63','IMAGES object (63)',3,'',18,1),(800,'2019-08-30 11:43:49.112535','64','IMAGES object (64)',1,'[{\"added\": {}}]',18,1),(801,'2019-08-30 13:12:59.137858','22','i-09bc0f076197310e9',1,'[{\"added\": {}}]',8,1),(802,'2019-08-30 13:24:03.065261','23','i-023704396f33ebb19',1,'[{\"added\": {}}]',8,1),(803,'2019-08-30 13:29:44.436461','24','i-02524c9ce5f522a50',1,'[{\"added\": {}}]',8,1),(804,'2019-08-30 13:48:33.035594','25','i-0b5447182955ccd11',1,'[{\"added\": {}}]',8,1),(805,'2019-08-30 13:51:39.559852','84','vol-0aba56a8e6ffaa068',1,'[{\"added\": {}}]',13,1),(806,'2019-08-30 16:59:32.976170','84','vol-0aba56a8e6ffaa068',3,'',13,1),(807,'2019-08-30 16:59:33.019023','83','vol-066e3cf04ef423422',3,'',13,1),(808,'2019-08-30 17:03:59.996220','26','i-0a7addf1607e7e794',1,'[{\"added\": {}}]',8,1),(809,'2019-08-30 17:06:26.336207','85','vol-07cb69f3ee9c0ce7d',1,'[{\"added\": {}}]',13,1),(810,'2019-08-30 17:18:46.342562','52','SNAPSHOTS object (52)',1,'[{\"added\": {}}]',19,1),(811,'2019-08-30 18:00:50.478692','27','i-0109033f627b9b17d',1,'[{\"added\": {}}]',8,1),(812,'2019-08-30 18:06:42.449764','86','vol-0727592da68666ad9',1,'[{\"added\": {}}]',13,1),(813,'2019-08-30 18:07:58.384368','65','IMAGES object (65)',1,'[{\"added\": {}}]',18,1),(814,'2019-08-30 18:12:17.479577','28','i-0c113539e38dc7abe',1,'[{\"added\": {}}]',8,1),(815,'2019-08-30 18:20:31.506281','87','vol-0a9fe87886ea3d346',1,'[{\"added\": {}}]',13,1),(816,'2019-08-30 18:20:56.985596','66','IMAGES object (66)',1,'[{\"added\": {}}]',18,1),(817,'2019-08-30 18:22:33.819167','53','SNAPSHOTS object (53)',1,'[{\"added\": {}}]',19,1),(818,'2019-08-30 18:28:15.443262','30','Mario',3,'',16,1),(819,'2019-08-30 18:31:34.871900','31','Adriano',1,'[{\"added\": {}}]',16,1),(820,'2019-08-30 18:32:54.556023','29','i-0c6045e25f3149c3e',1,'[{\"added\": {}}]',8,1),(821,'2019-08-30 18:34:19.558964','88','vol-0c7bb8f7994409617',1,'[{\"added\": {}}]',13,1),(822,'2019-08-30 18:35:07.998760','67','IMAGES object (67)',1,'[{\"added\": {}}]',18,1),(823,'2019-08-30 18:36:09.708751','54','SNAPSHOTS object (54)',1,'[{\"added\": {}}]',19,1),(824,'2019-08-30 18:37:06.487141','54','SNAPSHOTS object (54)',3,'',19,1),(825,'2019-08-30 18:37:35.044823','67','IMAGES object (67)',3,'',18,1),(826,'2019-08-30 18:38:05.223807','88','vol-0c7bb8f7994409617',3,'',13,1),(827,'2019-08-30 18:38:16.121585','88','vol-0c7bb8f7994409617',3,'',13,1),(828,'2019-08-30 18:39:01.952095','29','i-0c6045e25f3149c3e',3,'',8,1),(829,'2019-09-02 12:04:51.200138','31','Adriano',3,'',16,1),(830,'2019-09-02 12:06:07.735158','32','teste',1,'[{\"added\": {}}]',16,1),(831,'2019-09-02 12:07:22.966311','30','i-031668f1836822b4f',1,'[{\"added\": {}}]',8,1),(832,'2019-09-02 12:12:34.183206','89','vol-0651d42bda789e96c',1,'[{\"added\": {}}]',13,1),(833,'2019-09-02 12:19:31.011881','55','SNAPSHOTS object (55)',1,'[{\"added\": {}}]',19,1),(834,'2019-09-02 12:23:01.953357','89','vol-0651d42bda789e96c',3,'',13,1),(835,'2019-09-02 12:23:49.205901','30','i-031668f1836822b4f',3,'',8,1),(836,'2019-09-02 12:25:16.264131','31','i-09bafc3d3e7664413',1,'[{\"added\": {}}]',8,1),(837,'2019-09-02 12:27:09.067994','90','vol-0e82559c8af50337f',1,'[{\"added\": {}}]',13,1),(838,'2019-09-02 12:27:40.941883','68','IMAGES object (68)',1,'[{\"added\": {}}]',18,1),(839,'2019-09-02 17:10:00.044970','33','Mario',1,'[{\"added\": {}}]',16,1),(840,'2019-09-02 17:11:17.765022','32','i-0ad18bc2883307742',1,'[{\"added\": {}}]',8,1),(841,'2019-09-02 17:12:31.919078','91','vol-0c64d790e825ff89f',1,'[{\"added\": {}}]',13,1),(842,'2019-09-02 17:12:56.617029','69','IMAGES object (69)',1,'[{\"added\": {}}]',18,1),(843,'2019-09-02 18:11:34.006174','1','admin',2,'[{\"changed\": {\"fields\": [\"user_permissions\"]}}]',4,1),(844,'2019-09-02 18:14:00.109589','1','admin',1,'[{\"added\": {}}]',3,1),(845,'2019-09-02 18:14:16.722080','1','admin',2,'[{\"changed\": {\"fields\": [\"groups\"]}}]',4,1),(846,'2019-09-02 18:17:17.291485','3','teste',2,'[{\"changed\": {\"fields\": [\"is_superuser\", \"groups\", \"user_permissions\"]}}]',4,1),(847,'2019-09-02 18:18:28.257394','3','teste',2,'[{\"changed\": {\"fields\": [\"password\"]}}]',4,1),(848,'2019-09-02 18:21:31.618912','91','vol-0c64d790e825ff89f',3,'',13,1),(849,'2019-09-02 18:21:31.659966','90','vol-0e82559c8af50337f',3,'',13,1),(850,'2019-09-02 18:22:23.758286','33','Mario',3,'',16,1),(851,'2019-09-02 18:22:23.800599','32','teste',3,'',16,1),(852,'2019-09-03 12:47:03.408554','34','teste1',1,'[{\"added\": {}}]',16,1),(853,'2019-09-03 18:40:47.249066','AKIAZMQ5T3NFDZN2OGOO','CREDENTIALS object (AKIAZMQ5T3NFDZN2OGOO)',3,'',7,1),(854,'2019-09-03 18:41:00.989899','teste','CREDENTIALS object (teste)',1,'[{\"added\": {}}]',7,1),(855,'2019-09-04 10:29:06.052931','teste','CREDENTIALS object (teste)',2,'[{\"changed\": {\"fields\": [\"nome\"]}}]',7,1),(856,'2019-09-04 10:37:34.525283','AKIAZMQ5T3NFDZN2OGOO','Funchal',2,'[{\"changed\": {\"fields\": [\"id\", \"chave\"]}}]',7,1),(857,'2019-09-04 10:37:47.261500','teste','Funchal',3,'',7,1),(858,'2019-09-04 11:25:42.593754','35','Mario',1,'[{\"added\": {}}]',16,1),(859,'2019-09-04 11:27:00.453608','33','i-0d339678d3a8eb230',1,'[{\"added\": {}}]',8,1),(860,'2019-09-04 11:27:19.555149','33','i-0d339678d3a8eb230',3,'',8,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(12,'amazon','article'),(7,'amazon','credentials'),(11,'amazon','historicalinstance'),(18,'amazon','images'),(8,'amazon','instance'),(16,'amazon','keys'),(9,'amazon','outscale_instance_types'),(10,'amazon','regions'),(19,'amazon','snapshots'),(13,'amazon','volumes'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(17,'dashboard','userdashboardmodule'),(14,'jet','bookmark'),(15,'jet','pinnedapplication'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-08-13 15:50:10.638493'),(2,'auth','0001_initial','2019-08-13 15:50:11.874304'),(3,'admin','0001_initial','2019-08-13 15:50:12.977408'),(4,'admin','0002_logentry_remove_auto_add','2019-08-13 15:50:13.186089'),(5,'admin','0003_logentry_add_action_flag_choices','2019-08-13 15:50:13.278411'),(6,'contenttypes','0002_remove_content_type_name','2019-08-13 15:50:13.620793'),(7,'auth','0002_alter_permission_name_max_length','2019-08-13 15:50:13.768538'),(8,'auth','0003_alter_user_email_max_length','2019-08-13 15:50:13.928775'),(9,'auth','0004_alter_user_username_opts','2019-08-13 15:50:14.029502'),(10,'auth','0005_alter_user_last_login_null','2019-08-13 15:50:14.179438'),(11,'auth','0006_require_contenttypes_0002','2019-08-13 15:50:14.258787'),(12,'auth','0007_alter_validators_add_error_messages','2019-08-13 15:50:14.358300'),(13,'auth','0008_alter_user_username_max_length','2019-08-13 15:50:14.501774'),(14,'auth','0009_alter_user_last_name_max_length','2019-08-13 15:50:14.649300'),(15,'auth','0010_alter_group_name_max_length','2019-08-13 15:50:14.800895'),(16,'auth','0011_update_proxy_permissions','2019-08-13 15:50:14.996974'),(17,'sessions','0001_initial','2019-08-13 15:50:15.217226'),(18,'jet','0001_initial','2019-08-22 18:12:27.824560'),(19,'jet','0002_delete_userdashboardmodule','2019-08-22 18:12:27.948913'),(20,'dashboard','0001_initial','2019-08-22 18:18:22.800109');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('0640kvdsyodreef3cey5s5seprs5308h','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-13 18:31:09.396343'),('1ipsh8fizu96o3wa1qwuhuhmu2nu23t0','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-16 18:01:08.005285'),('2w6d05eey9p9rexqjmtdrh221t9alhya','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-17 13:33:57.007669'),('3999oj1qe2e5u4tmqx94q7vh1cicosh5','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-08-27 16:55:50.535877'),('3d27qy2zsgzlcnim01xfrpilpcd3wfoi','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-08-27 15:54:17.183902'),('5140qk2nwrtp6y44l2v4js91j7pcwh56','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-16 18:18:59.140646'),('62ph5j9us1ommd9wad11j32rp0fdkobe','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-17 15:51:50.979236'),('96rz1bvu7r9j5fha7jctzimeuxv0uoua','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-18 12:08:43.305478'),('fqk49qsbc79ruowklt5jm255kgsjgurz','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-09 11:40:25.046601'),('geouxkgcaf3aomz1v015ucv18mk4fsm8','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-02 10:29:22.129080'),('i8u1ggg3loxzntx701gnllzzxmjwrf2s','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-16 13:35:38.338178'),('j3r5yvlwy6ky9paa42y336ydz0zkjtcg','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-17 17:16:58.421156'),('jcdurtetclzh8smczpf0xatd1378gxdl','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-08-28 17:39:49.760471'),('k51lfz59nk78odjqwfpg73vr4uslntqy','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-08-28 14:04:41.224902'),('k6rksodyoxup7gsc9utbt125wqhc10mq','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-02 10:33:55.676169'),('lq1hiiwvv4xn9d7khl9vbcxiw7be0kh8','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-13 10:27:38.019693'),('m99sq0jjn9n9892qjz7kbld0g9ox2tz8','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-16 18:12:29.301049'),('nkjbn3ekf0s8tgtrguugah6jdft9hiik','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-16 17:59:24.114078'),('p8hy57g2wqpcjraw3nqqb5bqsi41n8xp','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-09 12:24:25.337364'),('vu33jcz8quhszkf38m6y78h2gyjo8qgz','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-17 12:45:26.993842'),('xteum98xphpakeoq7l8rauzqkgzdj4zj','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-17 17:17:16.112106'),('yb1kp4fcnklh6zjzwoauu5pxtsghsyod','NmZkYzBmMzBiODlhYjRmZTNkNDgxOWEyM2MxNGM0MWNkZDQxOWMzZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZmQzY2ZlODE3YjQ0NWEzOTQ4YzNhNTFhMTAxN2I2NGM0MDU1ZDgzIn0=','2019-09-17 18:44:32.840138');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jet_bookmark`
--

DROP TABLE IF EXISTS `jet_bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jet_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) NOT NULL,
  `title` varchar(255) NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `date_add` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jet_bookmark`
--

LOCK TABLES `jet_bookmark` WRITE;
/*!40000 ALTER TABLE `jet_bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `jet_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jet_pinnedapplication`
--

DROP TABLE IF EXISTS `jet_pinnedapplication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jet_pinnedapplication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(255) NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `date_add` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jet_pinnedapplication`
--

LOCK TABLES `jet_pinnedapplication` WRITE;
/*!40000 ALTER TABLE `jet_pinnedapplication` DISABLE KEYS */;
INSERT INTO `jet_pinnedapplication` VALUES (1,'amazon',1,'2019-08-22 18:44:50.794208');
/*!40000 ALTER TABLE `jet_pinnedapplication` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-04 11:24:52
